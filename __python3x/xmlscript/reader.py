import xml.etree.ElementTree as ET
#from pystring import clean,convert_dict
from dataModel import getClass
import sys,os
#from .mapper import Mods_Mapper_Main
#MAPPER=Mods_Mapper_Main()

#========================================================================

def logs(node,cls,args):

#========================================================================

    if node is not None:
        print("ERROR",node.path(),cls,args)
    else:
        print("ERROR","/",cls,args)
#========================================================================

def tree_from_stream(**kwargs):

#========================================================================

    #if sys.stdin.closed != True:
    string= input()
    print(string)
    return tree_from_string(string,**kwargs)

#========================================================================

def tree_from_string(string,**kwargs):

#========================================================================

    return tree_from_xml(ET.fromstring(string),**kwargs)
#========================================================================

def tree_from_file(filename,**kwargs):

#========================================================================

    #print(filename)

    #if os.path.isfile(filename)==True:

    return tree_from_xml(ET.parse(filename).getroot(),**kwargs)
       

    
#========================================================================

def tree_from_xml(xmlnode,parent=None,**kwargs):

#========================================================================


    if hasattr( xmlnode , 'attrib'): 
        attrib=dict(xmlnode.attrib )
        #attrib=dict(convert_dict(xmlnode.attrib ))
    else:
        attrib=None

    #tag
    if hasattr( xmlnode , 'tag'): 
        tag=xmlnode.tag

    else:
        tag=None

    #text
    if hasattr( xmlnode , 'text'):

        if xmlnode.text is not None:
 
            text=xmlnode.text.strip()

            if text !="":
                attrib["text"]=text

    #convert attributes
    attrib.update(kwargs)

    for k,v in attrib.items():
        try:
            attrib[k]=v#eval(v)
        except:
            pass

    #instance
    try:
        cls=getClass(tag)
    except:
        print("ERROR",tag)
        cls=getClass("Object")

    #print(cls,attrib)
    node=cls(parent=parent,**attrib)
    #node.onInit()
    #children
    """
    try:
        cls=getClass(tag)

        #print(cls,attrib)
        node=cls(parent=parent,**attrib)
        node.onInit()
        #children


    except:
        #cls=None
        cls=getClass("Log")
        attrib["result"]=False
        attrib["message"]="Erreur : classe "+tag
        node=cls(parent=parent,**attrib)
        print("XXX",tag)
        #logs(parent,tag,attrib)
        #raise Exception()
"""

    for xmlchild in xmlnode:
        tree_from_xml(xmlchild,parent=node)
    return node

#========================================================================







