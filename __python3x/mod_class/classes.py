
from mod_program import Node_Handler
from dataModel import *
import os
#TODO:decorator à changer
from .decorators import store
from mod_program import Object,Relation
#========================================================================

class Classes_InstanceOf(Relation):

#========================================================================

    #-------------------------------------------------------------
    def onSetup(self):
        pass
    #-------------------------------------------------------------
#========================================================================

class Classes_SubClassOf(Relation):

#========================================================================

    #-------------------------------------------------------------
    def onSetup(self):
        pass
    #-------------------------------------------------------------
#========================================================================

class Classes_Node(Object,Node_Handler):

#========================================================================

    pass

#========================================================================

class Classes_Attribute(Classes_Node):

#========================================================================
    def check_instance(self,node):
        pass

#========================================================================

class Classes_View(Classes_Node):

#========================================================================
    RENDER=Function()
    def render(self):
        return

#========================================================================

class Classes_Method(Classes_Node):

#========================================================================
    def call(self,node):
        pass



#========================================================================

class Classes_Class(Classes_Node):

#========================================================================

    CLASSES=Function() 
    SUBCLASSES=Function() 
    INSTANCES=Function() 

    ALLCLASSES=Function() 
    ALLSUBCLASSES=Function() 
    ALLINSTANCES=Function() 

    CLS_METHODS=Function()
    CLS_VIEWS=Function()
    CLS_ATTRIBUTES=Function()

    #-------------------------------------------------------------
    def onSetup(self):
        pass

    #-------------------------------------------------------------
    def classes(self):
        return self.links(outputs=True,cls=Classes_SubClassOf).targets()
    #-------------------------------------------------------------
    def subclasses(self):
        return self.links(inputs=True,cls=Classes_SubClassOf).subjects()

    #-------------------------------------------------------------
    @store
    def allclasses(self):
        for cls in self.classes():
            yield cls
            yield cls.allclasses()
    #-------------------------------------------------------------
    @store
    def allsubclasses(self):
        for cls in self.subclasses():
            yield cls
            yield cls.allsubclasses()

    #-------------------------------------------------------------

    def instances(self):
        return self.links(inputs=True,cls=Classes_InstanceOf).subjects()
    #-------------------------------------------------------------
    @store
    def allinstances(self):
        yield self.instances()
        for cls in self.allsubclasses():
            yield cls.instances()

    #-------------------------------------------------------------
    def instance(self,node,**args):
        return Classes_InstanceOf(source=node,target=self,**args)
    #-------------------------------------------------------------
    @store
    def check_instance(self,node):
        for attr in self.attributes():
            yield attr.check_instance(node)

    #-------------------------------------------------------------

    def cls_methods(self):
        return self.all().by_class(Classes_Method)

    #-------------------------------------------------------------

    def cls_views(self):
        return self.all().by_class(Classes_View)
    #-------------------------------------------------------------

    def cls_attributes(self):
        return self.all().by_class(Classes_Attribute)
    #-------------------------------------------------------------



#========================================================================

class Classes_Instance(Object):

#========================================================================
    CLASSES=Function()
    ALLCLASSES=Function()

    METHODS=Function()
    METHOD_EXEC=Function()
    VIEWS=Function()
    ATTRIBUTES=Function()

    #-------------------------------------------------------------
    def onSetup(self):
        pass
    #-------------------------------------------------------------
    def classes(self):
        return self.links(outputs=True,cls=Classes_Class).objects()
    #-------------------------------------------------------------
    @store
    def allclasses(self):
        for cls in self.classes():
            yield cls
            yield cls.allclasses()
    #-------------------------------------------------------------
    @store
    def methods(self):
        for cls in self.allclasses():
            yield cls.name,cls.methods()

    #-------------------------------------------------------------
    @store
    def method_exec(self,action=None,**args):
        for cls,methods in self.methods():
            for method in methods:
                if method.name == action:
                    return method.call(self,**args)

    #-------------------------------------------------------------
    @store
    def views(self):
        for cls in self.allclasses():
            yield cls.name,cls.views()
    #-------------------------------------------------------------
    @store
    def attributes(self):
        for cls in self.allclasses():
            yield cls.name,cls.attributes()
    #-------------------------------------------------------------
    @store
    def check_instance(self):
        for cls in self.allclasses():
            yield cls.name,cls.check_instance(self)

    #-------------------------------------------------------------
#========================================================================

class Classes_Group(Classes_Node):

#========================================================================
    CLASSES=Function()
    #-------------------------------------------------------------

    def classes(self):

        return self.children.by_class(Classes_Class)


         
    #-------------------------------------------------------------

    def get_class(self,name=None,cls=Classes_Class,**args):

        return self.append(name,cls=cls,**args)

#========================================================================

class Classes_Manager(Classes_Node):

#========================================================================
    GROUPS=Function()
    CLASSES=Function()
    INSTANCES=Function()

    #-------------------------------------------------------------

    def groups(self):

        return self.children.by_class(Classes_Group)

    #-------------------------------------------------------------

    def get_group(self,name=None):
        return self.append(name,cls=Classes_Group)

    #-------------------------------------------------------------
    @store
    def classes(self):

        for group in self.groups():
            yield group.classes()


         
    #-------------------------------------------------------------

    def get_class(self,name=None,separator="_",cls=Classes_Group,**args):
        #print(name)
        if separator in name:

            elts=name.split(separator)
            if len(elts)==2:
                group,name=elts
            else:
                group=elts[1]
                name=separator.join(elts[1:])
        else:
            group="Default"

        return self.get_group(group).get_class(name=name,cls=cls,**args)


    #-------------------------------------------------------------
    @store
    def instances(self):

        for cls in self.classes():
            yield cls.instances()

    #-------------------------------------------------------------

    def instance(self,node,classname=None):
        cls=self.get_class(classname=classname)
        if cls is None:
            return
        return cls.instance(node)
    #-------------------------------------------------------------

#========================================================================
                       
