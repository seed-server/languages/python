# -*- coding: utf-8 -*-
from .functions import Decorator
from .behaviors import Base_Node
from .frame import Frame
from .model import Model

#=============================================================================

def process_list(group,elt):

#=============================================================================

    for child in elt:
        process_result(group,child)

#=============================================================================

def process_result(group,elt):

#=============================================================================

    if elt is None:
        pass

    elif type(elt) is dict:
        group.append(Model(**elt) )  

    elif isinstance(elt,Model) == True:
        group.append(elt )  

    elif type(elt) is list or hasattr(elt,"__iter__")==True:
        process_list(group,elt)

    else:
        raise Exception("bad type",elt)

#=============================================================================

class ReturnNode(Decorator):

#=============================================================================


    def onCall(self,function,args,kwargs,Class=None,**options):

        return node(Class)(function)(*args,**kwargs)

#=============================================================================

class ReturnFrame(Decorator):

#=============================================================================


    def onCall(self,function,args,kwargs,Class=Frame,**options):

        return frame(Class)(function)(*args,**kwargs)

#=============================================================================

class Merge(Decorator):

#=============================================================================

    #-----------------------------------------------------------------

    def onCall(self,function,args,kwargs,**options):

        response = self.returnResult(function(*args, **kwargs))
        group=Frame()

        for elt in response:
            group.append(elt)  
              
        return group

    #-----------------------------------------------------------------
    def returnResult(self,result):

        if result is None:
            pass

        elif isinstance(result,Base_Node) :
            yield result

        elif isinstance(result,Frame) or (hasattr(result,"__iter__") and not hasattr(result,"__len__")):

            for elt in result:
                for elt2 in self.returnResult(elt):
                    yield elt2

    #-----------------------------------------------------------------

#=============================================================================

def node(cls):

#=============================================================================
    """
    décorateur pour les fonctions

    permet de créer un modele.
    la fonction doit renvoyer un dictionnaire ou itérateur ::

        @node(Model)
        def f():
            return dict()

        @node(Model)
        def f():
            for elt in lst:
                yield k,v
    """
#=============================================================================

    def decorated(func):

        def wrapper(*args, **kwargs):

            # Pré-traitement
            response = func(*args, **kwargs)
            response=cls(**response)
            return response
            # Post-traitement
        return wrapper
    return decorated

#=============================================================================

def frame(Frame=Frame,Model=Model):

#=============================================================================
    """
    décorateur pour les fonctions

    permet de créer une liste de modeles.
    la fonction doit renvoyer itérateur de modèles::

        @frame(Frame)
        def f():
            return [Model(),Model(),Model()]

        @frame(Frame)
        def f():
            for elt in lst:
                yield Model()
    """
#=============================================================================

    def decorated(func):

        def wrapper(*args, **kwargs):

            # Pré-traitement
            response = func(*args, **kwargs)

            group=Frame()

            for elt in response:
                process_result(group,elt)
            # Post-traitement
            return group
        return wrapper
    return decorated

#=============================================================================

def merge(Frame=Frame,Model=Model):

#=============================================================================
    """
    décorateur pour les fonctions

    permet de créer une liste de modeles.
    la fonction doit renvoyer itérateur de frame::

        @frame(Frame)
        def f():
            return [Model(),Model(),Model()]

        @merge()
        def g():
            for elt in lst:
                yield f()
    """
#=============================================================================

    def decorated(func):

        def wrapper(*args, **kwargs):

            group=Frame()

            for elt in func(*args, **kwargs):
                group.extend(elt)  
            
            return group

        return wrapper
    return decorated


#=============================================================================

def modelList(lst,cls):

#=============================================================================
    """
    décorateur pour les fonctions

    permet de créer une liste de modeles.
    la fonction doit renvoyer un itérateur de dictionnaire ::

        @modelList(Frame,Model)
        def f():
            return [dict(),dict(),dict()]

        @modelList(Frame,Model)
        def f():
            for elt in lst:
                yield dict()
    """
#=============================================================================
    def decorated(func):

        def wrapper(*args, **kwargs):

            # Pré-traitement
            response = func(*args, **kwargs)

            group=lst()

            for elt in response:
                group.append(cls(**elt))  
              
            # Post-traitement
            return group
        return wrapper
    return decorated

#=============================================================================

def output_dict():

#=============================================================================
    """
    décorateur pour les fonctions

    """
#=============================================================================
    def decorated(func):

        def wrapper(*args, **kwargs):

            # Pré-traitement
            response = dict(func(*args, **kwargs))

            return response
        return wrapper
    return decorated

#=============================================================================


