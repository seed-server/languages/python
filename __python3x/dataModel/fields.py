# -*- coding: utf-8 -*-

#=============================================================================

class Attribute(object):

#=============================================================================

    """
    L'attibut est créé par le champs Field correspondant.
    il n'est jamais créé directement.

    son nom est en minuscule

    gère les callback


    """
    #--------------------------------------------------------------------------
    def __init__(self, node,field):
        """
        creer un attribut pour le dictionnaire node avec le champs field.

        Attribut gère les appels à la valeur réelle :

           - contient la valeur de l'attribut
           - les fonctions réelles du noeud node sont assignées au fonctions callback
        """

        #instance attributes

        self.node = node
        self.field = field
        self.value = None


        #get real callback functions

        #modify
        if self.field.set_f is not None:
            self.set_f=getattr(self.node,self.field.set_f)
        else:
            self.set_f=None

        #access
        if self.field.get_f is not None:
            self.get_f=getattr(self.node,self.field.get_f)
        else:
            self.get_f=None

        #delete
        if self.field.del_f is not None:
            self.del_f=getattr(self.node,self.field.del_f)
        else:
            self.del_f=None
    #--------------------------------------------------------------------------

    def init(self,value=None):
        """
        valider les valeurs assignées aux champs
        """
        self.modify(value)
        #print("set value",self.node.__class__.__name__,self.field.name,self.value)

    #--------------------------------------------------------------------------

    def is_valid(self,value):

        #acceptable value None
        if value is None:
            return True

        #if a class declared
        elif  self.field.cls is not None:

            #if a list of classes
            if type(self.field.cls)==list:
                for c in cls:
                    if isinstance(value, c)==True:
                        return True

            #if single class
            else:
                return isinstance(value, self.field.cls)

        #no constrain, ok
        else:
            return True

        return False

    #--------------------------------------------------------------------------

    def validate(self):
        """
        valider les valeurs assignées aux champs
        """
        return self.is_valid(self.access())
    #--------------------------------------------------------------------------

    def access(self):

        "fonction executée quand l'attibut est appelé, retourne la valeur"

        if self.get_f is not None:
            return self.get_f(self.value)
        else:
            return self.value

    #--------------------------------------------------------------------------

    def modify(self,value):

        "fonction executée quand l'attibut est modifié, prend en argument la valeur"
        #print("modify",value)
        v=self.field.get_value(value)

        if v is not None:
            value=v
        elif type(value) in [str] and self.field.eval_expr == True:
            value=eval(value)
            

        #if no data
        #----------------------------
        if value is None:


            #if hasattr(self.node.__class__,self.field.name.upper()+"_DEFAULT"):
            
            #    self.value=getattr(self.node.__class__,self.field.name.upper()+"_DEFAULT")
            #    return

            #si valeur par defaut
            if self.field.default is not None:

                self.value=self.field.default
                return

            #ou nvl instance
            elif  self.field.create == True:

                self.value=self.field.cls()
                return


            #sinon c'est nul
            else:
                self.value=None
                return 

        #if set_f function,execute
        #----------------------------
        elif self.field.set_f is not None:
            self.value=self.set_f(value)
            return

        #si cls,travailler avec cls
        #----------------------------
        elif  self.field.cls is not None:

            #si instance de cls
            #if a list of classes
            if type(self.field.cls)==list:
                for c in self.field.cls:
                    if isinstance(value, c)==True:
                        self.value=value#self.field.cls[0](value)
                        return

            #if single class
            elif isinstance(value, self.field.cls):
                self.value=value
                return 

            #evaluer et retester
            else:

                if type(self.field.cls)==list:
                    cls=self.field.cls[0]
                elif isinstance(value, self.field.cls):
                    cls=self.field.cls

                try:
                    new_value=self.field.cls(value)
                    if type(new_value)==self.field.cls:
                        self.value=new_value
                        return
                except:
                    pass

                try:
                    self.value=cls(value)
                    return
                except:
                    pass
        #si valeur et pas de classe, retourne la valeur
        else:

            self.value= value
            return

        self.value= value
        #raise Exception("not good type for ",self.node,self.field.name,value)
        if type(value)==str and "$" not in value:
            print("ERROR : not good type for ",self.node.name,self.field.name,value)

    #--------------------------------------------------------------------------

    def delete(self):

        "fonction executée quand l'attibut est supprimé"

        if self.del_f is not None:
            self.value=self.del_f(self.value)
        else:
            pass

    #--------------------------------------------------------------------------

    def __str__(self):
        return self.value
    #--------------------------------------------------------------------------

#=============================================================================

class Field(object):

#=============================================================================

    """
    gestion des données et des types.

        comportement des attibuts

            - valeur par défault
            - type
            - fonctions attachées pour
                - accès
                - modification
                - suppression
    """
    CLASS=None
    Attribute=Attribute
    #--------------------------------------------------------------------------

    def __init__(self,cls=None,default=None,set_f=None,get_f=None,del_f=None,record=True,create=False,eval_expr=False):
        """

        """

        if cls is not None:
            self.cls = cls

        elif self.CLASS is not None:
            self.cls=self.CLASS

        else:
            self.cls=None

        self.name=None

        self.set_f=set_f
        self.get_f=get_f
        self.del_f=del_f

        self.create=create
        self.record=record
        self.eval_expr=eval_expr

       
        self.default=default

    #--------------------------------------------------------------------------

    def get_value(self,value):

        """
        récuperer la bonne valeur pour l'attibut.

        plusieurs cas :

            valeur nulle
                et valeur par défaut -> default
                et cls ->  cls()
                sinon -> None

            si valeur
                et cls ->  valeur
                sinon eval 
                    retest cls-> valeur
        """

        pass

    #--------------------------------------------------------------------------

    def new_attribute(self,node):

        """
        crée un nouvel attribut pour le noeud node avec la valeur transmise
        au constructeur. Cette valeur peut être nulle
        """

        #la première fois le dictionnaire stocke l'objet
        elt= self.Attribute(node,self)
        elt.init()
        node[self.name] =elt
        #ensuite, il met en place les callback


    #--------------------------------------------------------------------------

#=============================================================================



