# -*- coding: utf-8 -*-
"""
le modèle Tree_Node doit etre utilisé avec la classe Model
pour fonctionner ::

   class XXX(Model,Tree_Node):pass
"""
from .fields import Parent,ChildrenFrame
from dataModel import Function,Decorator,Field,Attribute,getClass
from dataModel import ModelMeta,Model,Field,Frame,String
from dataModel.decorators import frame

#TODO:rootpath and ancestors doublon


#=======================================================================

class Tree(metaclass=ModelMeta):

#=======================================================================
    #__metaclass__ = ModelMeta


    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """
    les noeuds sont liés par des relations parents/enfants
    qui permettent de naviguer dans la structure.
    """
    #---------------------------------------------------

    # INIT
    #==================================================

    #---------------------------------------------------

    NAME=String(eval_expr=False)
    """
    un identifiant pour construire les chemins
    """
    #---------------------------------------------------

    PARENT=Parent(record=False)

    """
    relation au noeud supperieur, peut etre nul (None) 

    recupérer la variable ::

       node = self.parent

    assigner un autre parent::

        self.parent=node

    détacher du parent ::

        self.parent=None
    """

    #---------------------------------------------------

    CHILDREN=Field(cls=ChildrenFrame,record=False,create=True)
    """
    liste des enfants ::

       for child in node.children:
           ...

    accès à toutes les fonctions de Frame ::

       node.children.by_class(CLS)
       node.children.by_attr(name="XXX")
       node.children.compare(lst)

    .. warning: restrictions sur append,extend,remove,delete
    """
    #---------------------------------------------------

    DETACH=Function()
    MOVE=Function(path=String())
    #ALL=Function()
    TREE=Function()
    SHOW=Function()
    CLEAR=Function()

    I=0

    #PARENT
    #==================================================


   #---------------------------------------------------

    def autoname(self):

        if self.name in [None,"None",""]:
            self.name=self.__class__.__name__+"_"+str(Tree.I)
            Tree.I+=1
        elif type(self.name) !=str:
            print(self.name)
            self.name=str(self.name)
            exit()
    #---------------------------------------------------

    def json_tree(self,**args):


        nodes=list()

        for elt in self.children:
            result=dict()
            result.update(args)
            result["text"]=elt.name
            result["nodes"]=elt.json_tree()
            nodes.append( result )


        return nodes


    # DESTROY
    #==================================================

    #---------------------------------------------------

    def onDestroy(self):
        "destroy node and detach it from tree"

        self.parent=None

        for child in list(self.children):
            child.destroy()


    #---------------------------------------------------

    def clear(self):
        "destroy all children and keep node"

        for child in list(self.children):
            child.destroy()

    #---------------------------------------------------

    def detach(self):
        self.parent=None

    #---------------------------------------------------

    def move(self,path=None):
        node=self.find(path)
        if node not in [self,self.parent,None]:
            if node not in self.all():
                self.parent=node

    #---------------------------------------------------

    def execOnElements(self,action,Class=None,recursive=False,**kwargs):
        "destroy all children and keep node"

        if recursive == True:
            lst=self.all()
        else:
            lst=self.children

        if Class is not None:
            lst=lst.by_class(Class)


        for child in list(self.children):
            if hasattr(child,action):
                yield getattr(child,action)(**kwargs)

   #---------------------------------------------------

    def query(self,action=None,**args):

        #print(action,args)
        if hasattr(self,action):
            r= getattr(self,action)(**args)
            return r



    #---------------------------------------------------
    @frame(Frame)
    def sorted_nodes(self):
        lst=list()
        for elt in self.children:
            lst.append((elt.name,elt))
        lst.sort()
        for name,elt in lst:
            yield elt
    #---------------------------------------------------

    # PATHES
    #==================================================

    #----------------------------------------------------------- 

    def rootpath(self,limit_node=None):
        "yield all node from current node to root node"
        yield self            
        if self!=limit_node:
            if not self.is_root:
                for elt in self.parent.rootpath(limit_node=limit_node):
                    yield elt

    #----------------------------------------------------------- 

    def rootpathReverse(self,**args):
        "yield all node from current node to root node"
        li= list(self.rootpath(**args))
        li.reverse()
        return li


   

    #-----------------------------------------------------------  

    def path(self,separator=u"/",reverse=False,root=False,relative=None):


        if  relative is not None:
            lst=[]
            for elt in self.rootpath(limit_node=relative):
                if elt == relative:
                    break
                lst.append(str(elt.name))

        else:
            lst=[ elt.name for elt in self.rootpath()]
            if root==False:

                lst=lst[:-1]
        lst.reverse()



        if reverse ==True:
            lst.reverse()

        #print(lst)

        return separator.join(lst)
    
    #-----------------------------------------------------  
    def append(self,elt_path,cls=None,separator="/",reverse=False,**args):
        #print(elt_path)
        if cls is None:
            cls=Tree
        elif type(cls)==str:
            cls=self.getClass(cls)
        node=self
        
        lst=elt_path.split(separator)

        if reverse==True:
            lst.reverse()
        
        for elt in lst:
            #print(elt_path,elt,list(node.children.keys()))
            if elt == "" :
                pass
            elif elt in node.children.keys() :
                node=node.find(elt)
            else:
                #print("new cls")
                node=cls(parent=node,name=elt)
        node.update(**args)
        return node
    
    #-----------------------------------------------------------  

    def find(self,path,separator=u"/",reverse=False,cls=None,error=True,**args):

        lst=path.split(separator)
        if reverse == True:
            lst=lst.reverse()

        if path.strip().startswith("/"):
            __root=self.root
        else:
            __root=self
            

        

        for name in lst:

            found=False
            if name=='..':
                __root=__root.parent
                found=True

            elif name=='.':
                found=True
                
            elif name=='':
                found=True

            else:

                for child in __root.children:

                    if str(child.name).strip() == name.strip():
                        __root=child
                        found=True
                        break

            if found == False:
                if cls is None:
                    if error == True:
                        return
                        #print("XXX no path %s in %s:%s"%(path,self.__class__.__name__,self.path()))
                        #raise Exception("no path %s in %s:%s"%(path,self.__class__.__name__,self.path()))

                    return None
                else:
                    __root=cls(parent=__root,name=name,**args)


        return __root
    #-----------------------------------------------------------  

    def findX(self,path,separator="/",reverse=False):

        parser=PathParser(separator=separator,reverse=reverse)
        lst=list(parser.to_list(path))


        if path.startswith("/"):
            __root=self.root
        else:
            __root=self
            

        for name in lst:

            found=False
            if name=='..':
                __root=__root.parent
                found=True

            elif name=='.':
                found=True

            else:

                for child in __root.children:

                    if str(child.name).strip() == name.strip():
                        __root=child
                        found=True
                        break

            if found == False:
                raise Exception("no path %s in %s:%s"%(path,self.__class__.__name__,self.path()))


        return __root

    #----------------------------------------------------------- 


    # TREE RELATIONS
    #==================================================

    #---------------------------------------------------

    @property
    def root(self):
        """
        accéder à la racine de l'arbre ::

           root=node.root
        """
        if self.parent is None:
            return self

        node = self
        while node.parent:
            node=node.parent

        return node

    #-----------------------------------------------------------
    @frame(Frame)
    def all(self):

        #yield self
        for child in self.children:
            yield child

            for elt in child.all():
                yield elt

    #-----------------------------------------------------------

    def iter_all(self):

        #yield self
        for child in self.children:
            yield child

            for elt in child.iter_all():
                yield elt
    #---------------------------------------------------

    @property
    @frame(Frame)
    def leaves(self):
        """
        accéder aux feuilles de l'arbre ::

           for leaf in node.leaves:
               ...
        """
        for node in self.descendants:
            if node.is_leaf:
                yield node

    #---------------------------------------------------

    @property
    @frame(Frame)
    def ancestors(self):
        """
        accéder aux ancetres du noeud ::

           for ancestor in node.ancestors:
               ...
        """
        node = self
        while node.parent:
            node=node.parent
            yield node

    #---------------------------------------------------
    def reverse_ancestors(self):
        """
        accéder aux ancetres du noeud ::

           for ancestor in node.ancestors:
               ...
        """
        r=list(self.ancestors)
        r.reverse()
        return r

    #---------------------------------------------------

    @property
    @frame(Frame)
    def descendants(self):
        """
        accéder aux descendants ::

           for descendant in node.descendants:
               ...
        """
        for child in self.children:
            yield child
            for elt in child.descendants:
                yield elt

    #-----------------------------------------------------------  


    #SITUATION
    #==================================================
    #---------------------------------------------------

    def is_inside(self,node):

        return node in self.ancestors

    #---------------------------------------------------
    @property
    def level(self):
        "renvoie la distance du noeud à la racine"

        return len(self.ancestors)

    #---------------------------------------------------
    @property
    def deph(self):
        "renvoie la distance max entre le noeud et la feuille la plus lointaine"

        maximum=0
        level=self.level
        for leaf in self.leaves:
            maximum=max(leaf.level-level,maximum)
        return maximum

    #-----------------------------------------------------------

    def size_ancestors(self):
        "nombre de descendants du noeud"
        return len(self.ancestors)

    #-----------------------------------------------------------

    def size_children(self):
        "nombre de descendants du noeud"
        return len(self.children)


    #-----------------------------------------------------------

    def size_descendants(self):
        "nombre de descendants du noeud"
        return len(self.descendants)

    #---------------------------------------------------
    @property
    def is_root(self):
        "retourne True si pas de parent"

        return (self.parent == None)

    #---------------------------------------------------
    @property
    def is_leaf(self):
        "retourne True isi pas d'enfants"

        return (len(self.children)==0)

    #---------------------------------------------------


    #ITERATORS
    #==================================================

    #-----------------------------------------------------------  
    @frame(Frame)
    def getLevel(self,level_max,level=0):

        if level_max==level:
            yield self
        else:
            for child in self.children:
                for elt in child.getLevel(level_max,level+1):
                    yield elt

    #-----------------------------------------------------------  

    def getLevels(self):

        for level in range(self.deph):
            yield level,list(self.getLevel(level))

    #-----------------------------------------------------------  
    @frame(Frame)
    def getDephArea(self,level_max,level=0):

        if level_max>=level:
            yield self
            for child in self.children:
                for elt in child.getDephArea(level_max,level+1):
                    yield elt

    #----------------------------------------------------------- 
    @frame(Frame)
    def getRange(self,deph_min,deph_max,level=0):

        if level>=deph_min:
            yield self

        if level<=deph_max-1:
            for child in self.children:
                for elt2 in child.getRange(deph_min,deph_max,level+1):
                    yield elt2


    #-----------------------------------------------------------  

    def getIntervals(self,divisions):

        depth=self.maxdepth
        level=0

        while level<depth:
            yield level,self.getRange(level,level+divisions-1)
            level+=divisions


    #CONSOLE
    #==================================================

    #---------------------------------------------------
    def tree(self,level=0,cls=None,limit=None):

        if type(cls) == list:
            test=False
            for elt in cls:
                
                if isinstance(self,elt):
                    test=True
            if test == False:
                return        
        elif cls:
            if cls and not isinstance(self,cls):
                return

        if type(limit) == str:
            limit=int(limit)


        if ( limit is not None and level<limit ) or (limit is None):

            if "name" in self.keys():
                name=self.name
            else:
                name=self.__class__.__name__

            string="#"+"    | "*level+" *  "+str(name)#+" - "+self.__class__.__name__
            print( string )
            level+=1

            for elt in self.children:

                elt.tree(level=level,limit=limit,cls=cls)

    #---------------------------------------------------
    def show(self,depth=0,limit=None):

        if type(limit)==str:limit=int(limit)
        if limit is not None and depth >= limit :
            return

        LINE_LEN=70
        CHAR_V="|"
        CHAR_H="_"

        SPACE=5
        
        SPACE_STRING=u" "*SPACE+CHAR_V
        SPACE_LINE=CHAR_H*LINE_LEN


        if "name" in self.keys():
            name=self.name
        else:
            name=self.__class__.__name__


        #print( SPACE_STRING*(deph) )
        print( SPACE_STRING*(depth)+" "*(SPACE+1)+SPACE_LINE )
        print( SPACE_STRING*(depth+1))

        print( SPACE_STRING*depth+SPACE_STRING,self.name,self.__class__.__name__ )
        print( SPACE_STRING*(depth+1)+SPACE_LINE )


        print( SPACE_STRING*depth+SPACE_STRING )

        DICT=dict(self.getRecordDict())


        for k,v in DICT.items():

            print( SPACE_STRING*depth+SPACE_STRING+"     ",k,v )
        #print( SPACE_STRING*(deph+1)+SPACE_LINE )


        for child in self.children:
            child.show(depth=depth+1,limit=limit)
            
        if len(self.children)>0:print( SPACE_STRING*(depth+1)+SPACE_LINE )
   #---------------------------------------------------

    def children_tree(self,level_max=3,cls=None):
        if cls is None:
            cls=Tree
        else:
            cls=getClass(cls)

        root=cls()
        for elt in self.children.by_class(cls):
            root.append(elt.name,separator=".",node=elt,cls=cls)

        return root


   #---------------------------------------------------
    def get_firsts(self,cls=None,exception=None,root=True):


        if root==True:
            if self.get_firsts_test(self,cls=cls,exception=exception)==True:
                yield self

            else:
                for elt in self.children:
                    for e in elt.get_firsts(cls=cls,exception=exception):
                        yield e
        else:
            for elt in self.children:
                for e in elt.get_firsts(cls=cls,exception=exception):
                    yield e

   #---------------------------------------------------
    def get_firsts_test(self,elt,cls=None,exception=None):

        if elt.is_instance(cls)==True:
            if exception is None:
                return True
            elif  elt.is_instance(exception)!=True:
                return True

        return

    #-------------------------------------------------
    def is_instance(self,cls):
        cls=self.getClass(cls)
        return isinstance(self,cls)
 
    #---------------------------------------------------



#=======================================================================


