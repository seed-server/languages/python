# -*- coding: utf-8 -*-
from dataModel import *

#=======================================================================

class LinkFrame(Frame):

#=======================================================================
    """
    = network?
    """

    #--------------------------------------------------------------------------
    def by_class(self,cls,**args):
        return LinkFrame(Frame.by_class(self,cls,**args))
    
    @frame(Frame)
    def subjects(self):
        for elt in self:
            yield elt.source
            
    @frame(Frame)
    def objects(self):
        for elt in self:
            yield elt.target

#=======================================================================

class NodeFrame(Frame):

#=======================================================================
    def nodes(self):
        pass
#=======================================================================

