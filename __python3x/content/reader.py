

from mod_program import Action
#import urllib2
from dataModel import *
from content  import DataSource,Content

#===============================================================

class Read(Action):

#===============================================================

    MIMETYPE=String()
    ROOT_ELEMENT=Content
    TREE_ELEMENT=Content

    def onNextCall(self,node):
        print(node,DataSource)
        if isinstance(node,DataSource):
            self.readNode(node)
        
        for elt in node.all().by_class(DataSource):
            self.readNode(elt)
    #-----------------------------------------------------

    def readNode(self,node=None,**args):
        return self.readString(content=node.read(),parent=node,**args)

    #-----------------------------------------------------

    def readString(self,content=None,parent=None,**args):

        if not parent:
            parent=self.ROOT_ELEMENT()

        for elt in self.onRead(content=self.onPreprocess(content=content)):
            #print(elt)
            self.TREE_ELEMENT(parent=parent,**elt)
        return self.onPostprocess(result=parent)

    #-----------------------------------------------------

    def onRead(self,content=None,**args):
        return content

    #-----------------------------------------------------

    def onPreprocess(self,content=None,**args):
        return content

    #-----------------------------------------------------

    def onPostprocess(self,result=None,**args):
        return result

    #-----------------------------------------------------
#===============================================================


