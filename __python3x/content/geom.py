from .numeric import *
from dataModel import *

#============================================================

class Vector2d(Vector):

#============================================================

    def onSetup(self):
        self.default=0.0
        self.x=self.set_data(name="0")
        self.y=self.set_data(name="1")


#============================================================

class Vector3d(Vector):

#============================================================

    def onSetup(self):
        self.default=0.0
        self.x=self.set_data(name="0")
        self.y=self.set_data(name="1")
        self.z=self.set_data(name="2")

