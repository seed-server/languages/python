from mod_program import Action

#===============================================================

class Write(Action):

#===============================================================


    def writeFile(self,filename=None,node=None,**args):

        f=open(filename,"w")
        content=f.write(self.writeString(node=node,**args))
        f.close()
        return 

    def writeString(self,node=None,**args):

        d.update(args)
        content=[]

        for elt in self.onWrite(node=node,**d):
            content.append(elt)
        return "\n".join(content)

    def onWrite(self,node=None):
        yield ""



#===============================================================
