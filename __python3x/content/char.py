from mod_program import Object

from dataModel import *
from .vector import List
import numpy as np
#============================================================

class CharArray(Object):

#============================================================

    X=Integer(default=1)
    Y=Integer(default=1)

    def onSetup(self):

        self.array=np.chararray((self.x,self.y))
        self.array[:]=self.default


    def plot(self):

        for i in range(self.x):
            line=""
            for j in range(self.y):
                line+=self.array[i,j].decode("utf-8") 
            print(line)


    def getLines(self):
        string=""
        for i in range(self.x):
            yield self.getLine(i)

    def setStringLine(self,i,string,offset=(0,0)):

        if i < (self.x-offset[0]):
            l=len(string)
            for j in range(l):
                self[offset[0]+i,offset[1]+j]=string[j]

    def getLine(self,i):
        string=""
        for j in range(self.y):
            string+=self[i,j]
        return string

    def setLine(self,i,char):
        for j in range(self.y):
            self[i,j]=char

    def getColumn(self,j):
        string=""
        for i in range(self.y):
            string+=self[i,j]
        return string

    def setColumn(self,j,char):
        for i in range(self.x):
            self[i,j]=char



    def borders(self,char):
        self.setLine(0,char)
        self.setLine(self.x-1,char)
        self.setColumn(0,char)
        self.setColumn(self.y-1,char)


    def add_matrix(self,sub_matrix,position):
        i=0
        for line in sub_matrix.getLines():
            self.setStringLine(i,line,offset=position)
            i+=1
#============================================================

