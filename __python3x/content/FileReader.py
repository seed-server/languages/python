
from dataModel import *
from .reader import Read
#===============================================================

class Read_Disc(Read):

#===============================================================


    #-----------------------------------------------------
    def readNode(self,node=None,**args):

        for elt in node.all().by_class("Disc_File"):
            r=self.readFile(elt)
            print(elt.name)
            r.url=node.file_path
            r.parent=self.root
    #-----------------------------------------------------
    def readFile(self,node):

        reader=self.getReader(node)
        print(reader)
        if reader:
            return reader.readNode(node=node)
    #-----------------------------------------------------
    def getReader(self,node):

        mimetype=node.mimetype()
        print(mimetype,self.children.__len__())
        for elt in self.children.by_class(Read):
            print(elt.mimetype,mimetype)
            if elt.mimetype==mimetype:
                #print(elt.mimetype,mimetype)
                return elt
    #-----------------------------------------------------

#===============================================================
