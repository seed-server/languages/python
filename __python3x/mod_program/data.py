

#===============================================================

class Code_Link(Model,Tree,Network_Node,Link):
    
#===============================================================



    def onSetup(self):
        #print(self.source,type(self.source))
        if type(self.source) == str:
            self.source= self.find(self.source)

        if type(self.target) == str:
            self.target= self.find(self.target)



#===============================================================

class Data(Model,Tree,Network_Node):
    
#===============================================================
    """
    base node for data
    """
    VALUE=Field(set_f="onSet",get_f="onGet",del_f="onDel")
    DEFAULT=Field(default=None)
    
    #---------------------------------------------------

    def onInit(self):
        "call setup and name"
        self.autoname()
        self.onSetup()

    #---------------------------------------------------
    def onSetup(self):
        "do nothing"
        pass

    #---------------------------------------------------
    def onSet(self,value):

        return value
    #---------------------------------------------------
    def onGet(self,value):

        return value
    #---------------------------------------------------
    def onDel(self,value):
        self.destroy()
        return
    #---------------------------------------------------
    def add(self,cls=None,value=None,**args):

        if not cls:cls=Data
        cls=self.getClass(cls)
        #print(self.path(),cls,value,args)
        node=cls(parent=self,**args)

        if value is not None:
            node.value=value
        elif self.default is not None:
            node.value=self.default
        #exit()
        return node
    #---------------------------------------------------
    def set_data(self,name=None,cls=None,value=None,error=False,**args):

        node=self.find(name,error=error)

        if node:
            if value is not None:
                node.value=value
            return node
        else:
            return self.add(cls=cls,value=value,name=name,**args)
    #---------------------------------------------------
