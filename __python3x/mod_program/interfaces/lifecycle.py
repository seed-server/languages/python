import os
from dataModel import *
from dataModelAddons.messages import *
from dataModelAddons import *

DEBUG=False
#==========================================================

class Tree_Initializer(Tree):
    
#==========================================================

    SETUP_DONE=Boolean(default=False,record=False)
    CLEANUP_DONE=Boolean(default=True,record=False)
    ACTIVE=Boolean(default=True)

    SETUP   =Function(TreeLogs())
    CLEANUP =Function(TreeLogs())

    #---------------------------------------------------
    def need_setup(self):
        return self.active==True and self.cleanup_done==True and self.setup_done==False

    #---------------------------------------------------
    def need_cleanup(self):
        return self.active==True and self.cleanup_done==False and self.setup_done==True

   #---------------------------------------------------
    def reset(self):
        self.cleanup()
        self.setup()

   #---------------------------------------------------
    def setup(self):
        """
        initialise node and children
        recursive behavior
        """

        if self.need_setup()==True :
            if DEBUG==True:print("SETUP",self.path(),self.__class__.__name__)
            yield self.onPreSetup()
            self.onSetup()
            yield self.children.by_class(Tree_Initializer).query(action="setup")
            self.setup_done=True
            self.cleanup_done=False
            yield self.onPostSetup()


   #---------------------------------------------------

    def cleanup(self):
        """
        terminate node and children
        recursive behavior
        """
        
        if self.need_cleanup() ==True:
            self.onPreCleanup()
            r=self.children.by_class(Tree_Initializer).query(action="cleanup")
            self.onCleanup()
            self.setup_done=False
            self.cleanup_done=True

            self.onPostCleanup()
            return r

    
    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        pass
    #---------------------------------------------------

    def onSetup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------


    def onPostSetup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------


    def onPreCleanup(self):
        "overload to have a behavior"
        pass


   #---------------------------------------------------

    def onCleanup(self):
        "overload to have a behavior"
        pass
    
   #---------------------------------------------------


    def onPostCleanup(self):
        "overload to have a behavior"
        pass

#==========================================================

class Tree_Runner(Tree_Initializer):
    
#==========================================================
    CYCLES_COUNTER=Integer(default=0)

    CALL   =Function(TreeLogs())
    RUN     =Function(TreeLogs())

    def run(self):
        yield self.setup()
        yield self.call()
        yield self.cleanup()


    def call(self):

        if self.active==True:
            self.cycles_counter+=1
            return self.onCall()

    def onCall(self):
        for elt in self.children.by_class(Tree_Runner):

            if elt.test()==True:
                yield elt.call()

#==========================================================
