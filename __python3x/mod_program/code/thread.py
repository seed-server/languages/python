import time,threading
from queue import Queue
from dataModelAddons import TreeLogs,treelogs,Log_Message
from .code import *

from dataModel import *

from multiprocessing.dummy import Pool as ThreadPool

#===================================================================

class Pool(SimpleCommand):

#===================================================================

    N=Integer(default=4)

    #----------------------------------------------------------------
    def onCall(self,**args):

        process=ThreadPool(self.n)
        result=process.map(self.threadCall, self.children.by_class("Code"))
        return result

    #----------------------------------------------------------------
    def threadCall(self,node):
        if node.test()==True:
            return node.call()
#===================================================================

class Thread(SimpleCommand):

#===================================================================
    """

    """
    START   =Function(TreeLogs())
    JOIN    =Function(TreeLogs())
    RESULTS    =Function()
    #----------------------------------------------------------------

    def onSetup(self,**args):
        self.result=None
        self.lock=threading.Lock()
        self.process=threading.Thread(target=self.onExecute)

    #----------------------------------------------------------------
    # BEHAVIOR
    #----------------------------------------------------------------

    def onExecute(self):
        """
        le fonction àà executer par le thread
        recupère le résultat dans self.result

        """
        self.result=self.onProcessLoop(**self)

    #----------------------------------------------------------------
    def onCall(self,**args):
        """
        si le thread n'a pas démarré, demarrer
        """
        if (self.process is not None) and (not self.process.isAlive() ):
            yield self.start(root=True)

        yield self.join(root=True)
        yield self.results()

    #----------------------------------------------------------------
    @treelogs
    def onProcessLoop(self,**args):
        """
        fonction intermediare pour ajouter le décorateur
        """

        return self.onProcess(**args)

    #----------------------------------------------------------------

    def onProcess(self,**args):
        """
        à surcharger
        par defaut : execute les actions qui ne sont pas des threads.
        """
        for elt in self.children.by_class(Code):
            if isinstance(elt,Thread) !=True:
                yield elt.call()

    #----------------------------------------------------------------
    
    def onCleanup(self,**args):
        """
        joindre tous les threads
        renvoie les résultats
        """
        yield self.join(root=True)
        yield self.results()

    #----------------------------------------------------------------
    # CONTROL
    #----------------------------------------------------------------
    def start(self,root=True):

        #print(self.path(),"start")

        if root == True:
            self.process.start()

        for elt in self.children.by_class(Thread):
            yield elt.start()

    #----------------------------------------------------------------
    def join(self,root=True):

        for elt in self.children.by_class(Thread):
            if elt.process and elt.process.isAlive():
                yield elt.join()

        if root == True:
            if self.process and self.process.isAlive():
                self.process.join()
        self.process=None

        #print(self.path(),"join")

    #----------------------------------------------------------------

    def results(self):

        if self.result is not None:
            result=self.result
            self.result=None
        else:
            result=Log_Message(name=self.name+'_result')

        for elt in self.children.by_class(Thread):
            r=elt.results()
            r.parent=result

        return result

    #----------------------------------------------------------------
    

#===================================================================

