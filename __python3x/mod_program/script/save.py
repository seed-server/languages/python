from ..atoms import *
from xmlscript import tree_to_file
import glob
from pathlib import Path
import minimal.path
#==========================================================

class Save_Xml(Action):
    
#==========================================================


    FILE_PATH=String()

    #-------------------------------------------------------
    def get_filepath(self,node):

    #-------------------------------------------------------
        if self.file_path:
            return self.file_path
        elif hasattr(node,"file_path"):
            return node.file_path


    #-------------------------------------------------------
    def onNextCall(self,node):

    #-------------------------------------------------------
        path=self.get_filepath(node)

        if path :
            tree_to_file(path,node)

    #-------------------------------------------------------



#==========================================================
