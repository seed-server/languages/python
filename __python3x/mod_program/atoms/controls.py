from dataModel import *
from ..interfaces import *
from ..nodes import Tree_Condition,Object,Code

#============================================================

class Action(Code,Action_Execution):

#============================================================
    #-------------------------------------------------
    def onTest(self):
        return self.onTestNode(self.get_node())  

    #-------------------------------------------------
    def onCall(self):

        return self.onNextCall(self.get_node())
    
    #-------------------------------------------------
  

#============================================================

class Call(Action):

#============================================================
    URL=String()

    #-------------------------------------------------
    def onNextCall(self,node):
        x=self.find(self.url)
        if x.onTestNode(node):
            return x.onNextCall(node)
        else:
            print("X"*15,"error when callling",self.url)
    
    #-------------------------------------------------
#==================================================================

class Echo(Action):

#==================================================================
    """

    """
    def onNextCall(self,node):
        print(self.text%node)#.get_infos())

#============================================================
