from .atoms import Set
from dataModel import String,Integer,Field

#============================================================

class IncrementInteger(Set):

#============================================================

    INCREMENT=Integer(default=0)

    #-------------------------------------------------
    def onSet(self,node):
        #print(node)
        return int(node[self.attr])+int(self.increment)
    
    #-------------------------------------------------

#============================================================

class IncrementTuple(Set):
    
#============================================================
  
    INCREMENT=Field(default=(0,0),eval_expr=True)
    #-------------------------------------------------
    def onSet(self,node):    
        x,y=node[self.attr]
        return int(x)+self.increment[0],int(y)+self.increment[1]
        
#============================================================

class IncrementColor(Set):
    
#============================================================
  
    INCREMENT=Field(default=(0,0,0),eval_expr=True)
    #-------------------------------------------------
    def onSet(self,node):    
        x,y,z,t=node[self.attr]
        return int(x)+self.increment[0],int(y)+self.increment[1],int(z)+self.increment[2],t+self.increment[3]
        
#============================================================
