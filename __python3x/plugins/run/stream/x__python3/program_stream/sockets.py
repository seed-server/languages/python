source="""
http://motoma.io/using-python-to-be-the-man-in-the-middle/
"""
import select
import socket
import time

BUFFER_SIZE=24
END="ANNADENN_END"
#====================================================================
class Connexion:

#====================================================================

    def __init__(self,conn):

        self.conn = conn

    def recv(self):

        result=""
        running = True
        while running:

            data = self.conn.recv(BUFFER_SIZE)

            if END in data:
                running = False
                data=data.replace(END,"")

            result+=data

        return result

    def send(self,data):
        self.conn.send(data)
        self.conn.send(END)

    def close(self):
        self.conn.close()

#====================================================================

class Listener:

#====================================================================

    def __init__(self,ip=None,port=None):

        self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listener.bind((ip, port))
        self.listener.listen(1)


    def accept(self):

        conn, addr = self.listener.accept()
        return addr,Connexion(conn)


    def close(self):
        self.listener.close()

#====================================================================

class Client(Connexion):

#====================================================================

    def __init__(self,ip=None,port=None):

        server = socket.socket()
        server.connect((ip, port))

        Connexion.__init__(self,server)

#====================================================================




