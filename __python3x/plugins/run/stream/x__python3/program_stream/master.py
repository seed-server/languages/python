import psutil,os
import shlex
from subprocess import PIPE

from atom import treelogs
from dataModel import String,Integer,Function
from IoNode import IoNode

#===============================================================

class Program_Master(IoNode):

#===============================================================
    """


    """
    Text=String()
    PID=Integer()

    #START=Function()
    #STOP=Function()
    #KILL=Function()


    #----------------------------------------------------------------
    def onReceiveMessage(self,message):
        self.stdin(string=str(message))

    #-----------------------------------------------------------
    #@treelogs
    def onStart(self):
        """

        """
        #print("start",shlex.split(self.text))
        self.__command_thread = psutil.Popen([self.text],stdin=PIPE , stdout=PIPE, stderr=PIPE, shell=True)

        self.pid=self.__command_thread.pid
        self.__command_thread_name= self.__command_thread.name()
        self.user_name = self.__command_thread.username()
        #print("start ok",self.__command_thread)
    #-----------------------------------------------------------

    def onStop(self):
        #print(self.path(),"onStop",self.is_command_running())
        if self.is_command_running() == True:
            #print(self.path(),"kill")
            self.command_kill()

            #print(self.path(),"kill ok",r)

        try:
            o, e = self.__command_thread.communicate()

            self.send_to_pipe(name="output",data=o)
            self.send_to_pipe(name="err",data=e)
        except:
            pass
        #self.send_output_message(self.stdout())
        #self.send_output_message(self.stderr())


    #-----------------------------------------------------------
    def onDo(self):
        #print(self.path(),"onDo ok")
        #print(self.path(),self.is_command_running())
        if self.is_command_running()==True:
            #print(self.path(),self.is_command_running(),self.command_status())
            #self.send_output_message(self.stdout_line())
            #self.send_output_message(self.stderr_line())

            try:
                o, e = self.__command_thread.communicate(timeout=1)

                self.send_to_pipe(name="output",data=o)
                self.send_to_pipe(name="err",data=e)
            except:
                pass
        else:

            self.stop()
    #----------------------------------------------------------------
    def onReceiveMessage(self,message):
        print(message)
        #self.stdin(string=message)


    #-----------------------------------------------------------
    # STREAMS
    #-----------------------------------------------------------

    def stdin(self,string=None,**args):
        self.__command_thread.stdin.write(string.encode())
        self.__command_thread.stdin.flush()


    #-----------------------------------------------------------
    def stdout(self):
        return self.__command_thread.stdout.read()

    #-----------------------------------------------------------
    def stdout_line(self):
        return self.__command_thread.stdout.readline()

    #-----------------------------------------------------------
    def stderr(self):
        return self.__command_thread.stderr.read()
    #-----------------------------------------------------------
    def stderr_line(self):
        return self.__command_thread.stderr.readline()

    #-----------------------------------------------------------
    #CONTROLS
    #-----------------------------------------------------------

    def send_query(self,path=None,distant_action=None,**args):

        if distant_action in [None,""]:
            return

        string=path+"?action="+distant_action
        for k,v in args.items():
            string+="&"+k+"="+unicode(v)
        self.stdin(string=string)

    #-----------------------------------------------------------

    def get_outputs(self,**args):

        if self.is_running() ==True:
            stdout,stderr = self.__command_thread.communicate("")
            return_code=self.__command_thread.returncode
            return Node(command=self.text,return_code=return_code,stdout=stdout,stderr=stderr)

        return Node(name="not_running")
    #-----------------------------------------------------------
    def get_outputs_tree(self,**args):
        node=self.get_outputs()
        

        try:
            output=from_string(node["stdout"],parent=node)
            node["stdout"]=None
        except:
            pass

        try:
            output=from_string(node["stderr"],parent=node)
        except:
            pass

        return node

    #-----------------------------------------------------------

    def command_wait(self,timeout=2):
        self.__command_thread.wait(timeout=timeout)

    #-----------------------------------------------------------

    def is_command_running(self):
        return self.__command_thread.is_running()

    #-----------------------------------------------------------
    def command_status(self):
        return self.__command_thread.status()

    #-----------------------------------------------------------

    def command_suspend(self):
        self.__command_thread.suspend()

    #-----------------------------------------------------------
    def command_resume(self):
        self.__command_thread.resume()

    #-----------------------------------------------------------
    def command_stop(self):
        self.__command_thread.terminate()

    #-----------------------------------------------------------
    def command_kill(self):
        """
        solution trouvee : https://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
        """
        process = psutil.Process(self.pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()

    #-----------------------------------------------------------

    def command_send_signal(self,signal):
        self.__command_thread.send_signal(signal)

    #-----------------------------------------------------------



#===============================================================



