from mod_program import *
from dataModel import *
from .Apps_Node import *
import os
from minimal.path import filterfiles,filterdirectories,FilePath
from tree_files import Disc_Directory
#===============================================================

class Apps_Container(Apps_Node):
    
#===============================================================
    DEFAULT_CLASS="Apps_Node"
    CLS=String(default=None)
    SEPARATOR=String(default=".")

    def onSetupNodes(self):pass

    #-------------------------------------------------------------
    def onSetup(self):
        Apps_Node.onSetup(self)
        if self.cls is None:
            self.cls_obj=getClass(self.__class__.DEFAULT_CLASS)
        else:
            self.cls_obj=getClass(self.cls)

        if self.is_ok():
            self.onSetupNodes()
            #self.fillAppTree()
    #-------------------------------------------------------------
    def append_structure(self,name=None,file_path=None,**args):

        app= self.append( name,
                            cls=self.cls_obj,
                            file_path=file_path,
                            **args)
        return app
    #-------------------------------------------------------------
    def fillAppTree(self):
        for node in self.children_tree(cls=Apps_Node).all():
            if "node" not in node.keys():
                name=node.path()
                name=name.replace("/",".")
                Apps_Node(parent=self,name=name)

    #-------------------------------------------------------------
#===============================================================

class Apps_XmlDir(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_RecursivePackage"

    def onSetupNodes(self):

        for elt in os.listdir(self.file_path):
            if not elt.startswith("__"):

                elt_path=os.path.join(self.file_path,elt)
                if os.path.isfile(elt_path):
                    ext=elt.split(".")[-1]
                    if ext == "xml":
                        name=elt.split(".")[0]
                        self.append_structure(  name=name,
                                                file_path=elt_path)
     

#===============================================================

class Apps_RecursivePackage(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_RecursivePackage"

    def onSetupNodes(self):

        for name,elt in iterApps(self.file_path):
            self.append_structure(  name=name,file_path=elt)

#===============================================================

class Apps_RecursiveFilter(Apps_Container):
    
#===============================================================

    FILTER=String(default="__init__.xml")

    def onSetupNodes(self):

        for elt in filterdirectories(self.file_path,self.filter,recursive=True):
            print(elt.join(self.filter))
            content=xmlscript.tree_from_file(elt.join(self.filter).string,name=elt.name(),parent=self)

            if hasattr(content,"setup"):
                content.processTree()
                result=content.setup()
                result.name="setup"
                result.parent=content
#===============================================================

class Apps_RecursiveDirFilter(Apps_Container):
    
#===============================================================

    FILTER=String(default="__init__.xml")

    def onSetupNodes(self):
        path=self.get_path()

        for elt in filterdirectories(path,self.filter,recursive=True):

            content=Apps_RecursivePackage(name=elt.root.name,parent=self,file_path=elt.string,setup=True)
            result=content.setup() 
            result.name="setup"
            result.parent=content                     
#===============================================================

class Apps_Store(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_Node"
    ROOT_PATH=Boolean(default=False)


    #-------------------------------------------------------------
    def onSetupNodes(self):

        for name,elt in iterApps(self.file_path):
            if self.root_path == True:

                self.append_structure(  name=name,
                                        app_path=self.name+"/"+name,
                                        file_path=elt,
                                    separator=self.separator)

            else:
                self.append_structure(  name=name,
                                        app_path=name,
                                        file_path=elt,
                                    separator=self.separator)
              
    
#===============================================================

class Apps_MultiStore(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_Store"

    #-------------------------------------------------------------
    def onSetupNodes(self):

        for name,elt in iterApps(self.file_path):
            self.append_structure(  name=name,
                                    app_path=name,
                                    file_path=elt,
                                    root_path=True,
                                    separator=self.separator) 
        self.fillAppTree()


#===============================================================

class Apps_FilterStore(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_Node"

    def onSetupNodes(self):

        for name,elt in iterApps(self.file_path):

            if name.startswith(self.name)==True:

                self.append_structure(name=name,app_path=name,file_path=elt)
            
    
#===============================================================

class System_Stores(Apps_Container):
    
#===============================================================
    DEFAULT_CLASS="Apps_Store"

    #-------------------------------------------------------------
    def onSetup(self):
        Apps_Container.onSetup(self)

        print("ok")
        lst=os.environ["SYSTEM_STORES"].split(":")

        for elt in lst:
            #print(elt)
            if os.path.isdir(elt):
                name=elt.split('/')[-1]
                print(name)
                self.append_structure(name=name, app_path=name,file_path=elt,separator=self.separator) 

#===============================================================
