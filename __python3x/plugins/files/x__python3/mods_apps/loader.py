from IoNode import IoNode_Atom
from dataModel import *
import time
from .instances import Apps_InstanceGroup
#============================================================

class Apps_Loader_Plugin(IoNode_Atom):

#============================================================
    PLUGINS=String()
    APPS=String()
    #-------------------------------------------------------

    def onExecute(self,**args):
        apps=self.find(self.apps)
        plugins=self.find(self.plugins)
        for filter_obj in plugins.all().by_class("Apps_Filter"):
            print("filter_obj",filter_obj.path())
            name=filter_obj.name
            elt=self.append(name,cls=Apps_InstanceGroup,
                        source=filter_obj,
                        target=apps)
            elt.setup()
            elt.on_update()
            time.sleep(0.05)
#============================================================

class Apps_Loader(IoNode_Atom):

#============================================================
    PLUGINS=String()
    APPS=String()
    #-------------------------------------------------------

    def onExecute(self,**args):

        apps=self.find(self.apps)
        plugins=self.find(self.plugins)

        for elt in apps.all().by_class("Apps_Store"):
            print(50*"#","app",elt.path())
            elt.query("on_update")
            time.sleep(0.05)

        for elt in plugins.all().by_class("Apps_Plugins"):
            print(50*"#","plug",elt.path())
            elt.query("on_update")
            time.sleep(0.05)

        for elt in self.root.all().by_class("IoNode_Atom"):

            if elt.is_running()!=True:
                print(50*"#",elt.path(),"start")
                elt.query(action="start")
                #elt.start()
            time.sleep(0.05)

        print(self.path(),"stop")
        self.query(action="stop")
    #-------------------------------------------------------

    def onExecuteX(self,**args):

        for plugin in plugins.all().by_class("Apps_Plugins"):
            print("plugins",plugin.path())
            for app in apps.all().by_class("Apps_Store"):
                name=plugin.name+"."+app.name
                node=Apps_Loader_Plugin(parent=self,name=name,plugins="/"+plugin.path(),apps="/"+app.path() )
                node.setup()
                node.start()
                time.sleep(0.05)

        for thread in plugins.by_class("IoNode_Atom"):
            print("thread",thread.path())
            thread.start()
            time.sleep(0.05)
        self.stop()

    #-------------------------------------------------------
#============================================================
