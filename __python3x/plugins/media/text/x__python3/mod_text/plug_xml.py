

import xml.dom.minidom
from content import Read,Write
from .plug_text import Read_CleanText,Read_Text
from mod_program import Data
from dataModel import *

#========================================================================

class Xml_Node(Data):

#========================================================================

    #----------------------------------------------------------------

    def onSetup(self):


        #print( self.xmlnode.nodeName)
        #print( self.xmlnode.localName)
        #print( self.xmlnode.namespaceURI)


        if self.xmlnode.localName:self.update(__xmlclass__=self.xmlnode.localName)
        else:self.update(__xmlclass__=self.xmlnode.nodeName)


        cls=self.get_class(self.xmlnode.namespaceURI,self.__xmlclass__)
        
        if self.xmlnode.attributes:
            for k,v in self.xmlnode.attributes.items():#.itemsNS():
                cls.get_attr(k)
                #namespace,cls=k
                #print( k,v )
                #self[k]=v
                #self.getProperty(cls,namespace=namespace)
                
        #if self.xmlnode.attributes:
        #    self.update(**self.xmlnode.attributes)

        if self.xmlnode.namespaceURI:self.update(__namespace__=self.xmlnode.namespaceURI)

        #children
        text=""

        for xmlchild in self.xmlnode.childNodes:

            if xmlchild.nodeName =="#text":
                text+=xmlchild.data
            else:
                Xml_Node(xmlnode=xmlchild,parent=self)

        text=text.replace("\n","")
        text=text.strip()
        if text !="":
            cls.get_attr("text")
            self.update(__text__=text)

        del self["xmlnode"]


    #----------------------------------------------------------------
    def get_class(self,ns,cls):

        if hasattr(self.parent,"get_ns"):
            return self.parent.get_class(ns,cls)
        else:
            #print(ns,cls)
            ns= self.get_ns(ns)
            return ns.get_class(cls)

    #----------------------------------------------------------------
    def get_ns(self,ns):
        if ns:
            for elt in self.parent.children.by_class(Xml_Ns):
                if elt.namespace==ns:
                    return elt
        return self.parent.append("default",cls=Xml_Ns)

#========================================================================

class Xml_Class(Data):

#========================================================================

    #----------------------------------------------------------------
    def get_attr(self,attr):
        cls= self.append(attr,cls=Data)
        return cls

#========================================================================

class Xml_Ns(Data):

#========================================================================

    SHORT=String()
    URI=String()
    NAMESPACE=Field()

    #----------------------------------------------------------------

    def onSetup(self):
        pass

    #----------------------------------------------------------------

    """
    def tree_from_xml(xmlnode,parent=None,includedoc=False,**kwargs):


        #recuperer les namespaces
        if xmlnode.firstChild.attributes:
            for k,namespace in xmlnode.firstChild.attributes.itemsNS():
                uri,short=k
                if uri == 'http://www.w3.org/2000/xmlns/':
                    mapper.getNamespace(namespace,origine=uri,short=short)
    """    

    #----------------------------------------------------------------
    def get_class(self,cls):
        cls= self.append(cls,cls=Xml_Class)
        return cls
#========================================================================

class Read_Xml(Read):

#========================================================================

    #----------------------------------------------------------------

    def readString(self,content=None,parent=None,**args):


        xmlnode = xml.dom.minidom.parseString(content)


        #namespaces
        if xmlnode.firstChild.attributes:
            for k,namespace in xmlnode.firstChild.attributes.itemsNS():
                #print(k,namespace)
                uri,short=k
                Xml_Ns(parent=parent,name=short,short=short,uri=uri,namespace=namespace)
        Xml_Node(parent=parent,xmlnode=xmlnode,**args)



#========================================================================

class Write_Xml(Write):

#========================================================================


    #----------------------------------------------------------------

    def writeString(self,node=None,**args):

        pass
    #----------------------------------------------------------------

#========================================================================

    






     
