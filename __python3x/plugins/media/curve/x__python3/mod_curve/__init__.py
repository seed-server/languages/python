__author__="GwikGwik"
__doc__="""
module curve pour manipuler des courbes les construire les transformer
"""
from .curve import *
from .hist import *
from .Array import *
from .Plot import *
