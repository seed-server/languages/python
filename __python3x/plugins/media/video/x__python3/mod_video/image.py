from atom import Atom
from mod_program import *
from content import Read,Write
from dataModel import *
import numpy as np
import cv2
from minimal.path import ls,filterfiles
from dataModel import *
from IoNode import IoNode

#===========================================================

class Image(Data):

#===========================================================

    IMG=Field()

    def onSetup(self):
        pass

#===========================================================

class Read_Image(Read):

#===========================================================

    FILE_PATH=String()

    def onSetup(self):
        pass

    def onRead(self):
        self.img = cv2.imread(self.file_path,-1)

#===========================================================

class Write_Image(Read):

#===========================================================

    def onWrite(self):
        cv2.imwrite(self.file_path,self.img)

#===========================================================

class Show_Image(Action):

#===========================================================

    def oncall(self):

        cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
        cv2.imshow(self.name,node.img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

#===========================================================

class OpenCV_ImageDirectory(IoNode):

#===========================================================


    FILE_PATH=String()
    EXTENTION=String()
    RECURSIVE=Boolean(default=False)

    def onStart(self):
        
        self.files=ls(self.file_path,recursive=self.recursive).filterfiles(self.extention).sort()
        #print(self.file_path)
        #print(self.files)

    def onDo(self):
        if len(self.files) >0:

            elt=self.files[0]
            self.files.remove(elt)
            #print(elt)
            self.send_video(elt.name(),cv2.imread(elt.string,-1) )
        else:
            self.root.stop()

     #---------------------------------------------------------------------------
    def send_video(self,msg,img):
        #print(message)
        for pipe in self.outputs:

            pipe.send_message((msg,img))
#===========================================================
