
########################################
## opencv for video
########################################


APT "libavcodec-dev"
APT "libavformat-dev"
APT "libswscale-dev"
APT "libv4l-dev"
APT "libxvidcore-dev"
APT "libx264-dev"
APT "libgtk-3-dev"
APT "libatlas-base-dev"
APT "gfortran"
APT "python3-dev"
PIP3 "matplotlib"
#PIP3 "opencv-python"
APT "libopencv-dev"
APT "python-opencv"
APT "python3-opencv"
SUDO "pip install matplotlib --upgrade --ignore-installed six"


APT "python-tk"
APT "python-imaging-tk" #pour la gestion des images sous tkinter
APT "python3-tk" #pour la version 3.x de python. (La version 3.x comprend les widgets ttk)

