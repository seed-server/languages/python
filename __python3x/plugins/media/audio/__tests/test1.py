import pygame
from time import sleep
from wave import x,fs

pygame.mixer.pre_init(fs, size=-16, channels=1)
pygame.mixer.init()
sound = pygame.sndarray.make_sound(x)

sound.play()

sleep(0.01) # NOTE: Since sound playback is async, allow sound playback to start before Python exits

