

import jinja2
from jinja2 import Environment, FileSystemLoader
from flask import Flask,render_template,request,redirect
from flask import Response
from flask import jsonify,send_from_directory
from flask_cors import CORS
from flask import Response


from dataModel import *
from mod_program import *
from IoNode import *
from xmlscript import tree_to_string
#===================================================================

class IoFlask_Request(IoServer_Request):

#===================================================================

    METHOD=String()
    QUERY_ARGS=String()
    #-------------------------------------------------------
    def onCleanArgs(self,**args):

        for k,v in args.items():
            if type(v)==list:
                if len(v)==1:
                    yield k,v[0]
                else:
                    yield k,v
            else:
                yield k,v

    #-------------------------------------------------------
    def setup(self):

        #self.url_args=args
        self.query_args=dict(self.onCleanArgs(**self.flask_request.args))
        self.url_method=self.flask_request.method
        self.response=""
        self.tree_node=None
        self.module=None
        self.method=None
        self.done=False
    #-------------------------------------------------------
    def call(self):
        return self.response
    #-------------------------------------------------------

#===================================================================

class IoFlask_Response(Object):

#===================================================================
    MIMETYPE=String(default='text')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = Response("", mimetype=self.mimetype)
    #-------------------------------------------------------
#===================================================================

class IoFlask_XmlResponse(IoFlask_Response):

#===================================================================

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = Response(tree_to_string(request), mimetype=self.mimetype)
    #-------------------------------------------------------
#===================================================================

class IoFlask_TemplateResponse(IoFlask_Response):

#===================================================================
    TEMPLATE=String(default='index.html')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = Response(render_template(self.template,node=request), mimetype=self.mimetype)
    #-------------------------------------------------------
#===================================================================

class IoFlask_RedirectResponse(IoFlask_Response):

#===================================================================
    REDIRECT=String(default='/')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = redirect(self.redirect)
    #-------------------------------------------------------

#===================================================================

class IoFlask_Method(Object,Action_Execution):

#===================================================================
    def onTestRequest(self,request):
        return request.url_method == self.name

    def onProcessRequest(self,request):

        if request.tree_node:
            node=request.tree_node
        else:
            node=self.root
            request.tree_node=self.root

        request.method=self
        for msg in self.onNextCall(node):
            msg.parent=request

        for response in self.children.by_class(IoFlask_Response):
            if response.onTestRequest(request):
                response.onProcessRequest(request)
                request.done=True
                break

#============================================================

class IoFlask_Shutdown(IoFlask_Method):

#============================================================


    def onProcessRequest(self,request):

        func = request.flask_request.environ.get('werkzeug.server.shutdown')
        if func:
            func()
            #print("server stop")
        request.done=True
        request.response= self.redirection("/")



#===================================================================

class IoFlask_Node(Object):

#===================================================================
    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        for method in self.children.by_class(IoFlask_Method):
            if method.onTestRequest(request):
                method.onProcessRequest(request)
                if request.done==True:break
#===================================================================

class IoFlask_Module(IoServer_Module):

#===================================================================
    SERVER_PATH=String()

    def onSetup(self):
        self.name=self.server_path[1:].replace("/","_")

    def get_path(self,request):

        if request.select==self.server_path:
            return ""

        path=request.select[len(self.server_path):]
        if path.startswith("/"):
            path=path[1:]
        return path

    def onTestRequest(self,request):
        return request.select.startswith(self.server_path)

    def onProcessRequest(self,request):
        pass

#===================================================================

class IoFlask_Location(IoFlask_Module):

#===================================================================

    def onProcessRequest(self,request):
        if request.done==True:return
        path=self.get_path(request)
        if path=="":path="index"
        print(path)
        node=self.find(path)
        if node:
            if node.onTestRequest(request)==True:
                request.module=self
                node.onProcessRequest(request)
            else:
                return
        
#===================================================================

class IoFlask_TreePath(IoFlask_Module):

#===================================================================

    ROOT_PATH=String(default="/")

    def onProcessRequest(self,request):
        if request.done==True:return
        root=self.find(self.root_path)
        path=self.get_path(request)
        print(self.root_path,path)

        if path=="":
            request.tree_node=root
        else:
            request.tree_node=root.find(path)

        for method in self.children.by_class(IoFlask_Method):
            if method.onTestRequest(request):
                request.module=self
                method.onProcessRequest(request)
                if request.done==True:break

#============================================================

class IoFlask_Static(IoFlask_Module):

#============================================================

    FILE_PATH=String()

    #-------------------------------------------------------
    def onProcessRequest(self,request):
        path=self.get_path(request)
        if path !="":
            url_path=self.server_path+"/"+path+"/"
            fullpath=os.path.join(self.file_path,path)

        elif os.path.exists(self.file_path+"/index.html"):
            url_path=self.server_path
            fullpath=self.file_path+"/index.html"
            path="index.html"
        else:
            url_path=self.server_path+"/"
            fullpath=self.file_path
            path=""

        if os.path.isfile(fullpath):
            request.response= send_from_directory(self.file_path,path)
            request.done=True

        elif os.path.isdir(fullpath):
            r=[]
            for elt in os.listdir(fullpath):
                r.append(url_path+elt)
            request.response= dict(result=r)
            request.done=True

    #-------------------------------------------------------

#============================================================

#============================================================

class IoFlask(IoServer):

#============================================================
    """
    root node for flask server, a tree of Server_Node to
    build an application
    """
    REQUEST_CLS=IoFlask_Request

    IP=String(default="localhost")
    PORT=Integer(default=8000)
    DEBUG=Boolean(default=True)
    TEMPLATE_FOLDER=String(default=None)
    #-------------------------------------------------------
    def onSetup(self):
    #-------------------------------------------------------
        #print("#"*50,self.path())

        #self.setEnviron()
        #self.setCommandLine()
        #self.processTree()


        self.app = Flask(self.name.replace(".","_"),template_folder=self.template_folder)
        CORS(self.app)
        self.app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1


        self.app.add_url_rule("/","main", self.ask,['GET','POST'])   
        self.app.add_url_rule("/<path:path>","main_path", self.ask_path,['GET','POST','PUT','DELETE'])   

        IoServer.onSetup(self)

    #-------------------------------------------------------
    def onPostSetup(self):
    #-------------------------------------------------------
        print("setup done")
        self.tree()

    #-------------------------------------------------------
    def onDo(self):
    #-------------------------------------------------------

        self.app.run(host=self.ip,
                        port=int(self.port),
                        debug=self.debug, 
                        threaded=True, 
                        use_reloader=False)


    #-------------------------------------------------------
    def ask(self,**args):
    #-------------------------------------------------------
        return self.onProcessRequest(select="/",flask_request=request,**args)

    #-------------------------------------------------------
    def ask_path(self,path,**args):
    #-------------------------------------------------------
        return self.onProcessRequest(select="/"+path,flask_request=request,**args)

    #-------------------------------------------------------




#============================================================
