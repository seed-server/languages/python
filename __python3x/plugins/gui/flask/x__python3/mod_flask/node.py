



from flask import Flask,request,redirect
from flask import Response
from flask import jsonify,send_from_directory
from flask import render_template
from flask_cors import CORS
import jinja2
from jinja2 import Environment, FileSystemLoader

from dataModel import *



from mod_program import Code_Block


#============================================================

class Api_Base(metaclass=ModelMeta):

#============================================================

    #-------------------------------------------------------
    def error(self,*args,**kwargs):

        return dict(error=True,url="/"+self.path(),args=args,kwargs=kwargs)
    #-------------------------------------------------------
    def redirection(self,path):

        return redirect(path)

    #-------------------------------------------------------
    def get_help(self):
    #-------------------------------------------------------
        result=dict()

        #result["class"]=self.__class__.__name__

        comments=self.get_comments()
        if len(comments)>0:
            result["description"]=comments

        for k,v in self.onHelp():
            result[ k ]=v
        return result

    #-------------------------------------------------------
    def onHelp(self):
    #-------------------------------------------------------
        pass

    #-------------------------------------------------------
    def get_comments(self):
    #-------------------------------------------------------
        comments=""
        
        for elt in self.children.by_class("Text"):
            comments+=elt.text+"\n"

        return comments

    def render_template(self,template,**kwargs):
        return render_template(template,**kwargs)
    #-------------------------------------------------------
    def get_mime(self,template):
    #-------------------------------------------------------
        return magic.from_file(self.root.template_folder+"/"+template,mime=True)


    #-------------------------------------------------------

#============================================================

class Api_Element(Code_Block,Api_Base):

#============================================================

    pass

