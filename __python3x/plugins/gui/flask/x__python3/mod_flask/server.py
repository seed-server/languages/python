

import jinja2
from jinja2 import Environment, FileSystemLoader
from flask import Flask,render_template,request
from flask import Response
from flask import jsonify,send_from_directory
from flask_cors import CORS



from dataModel import *




import urllib.request,os,time
import xmlscript,time
from .node import Api_Base
from .modules import Api_Module
from .api import *
from mod_program import Code
from IoNode import *

#============================================================

class Api_Main(IoNode,Api_Base):

#============================================================
    """
    root node for flask server, a tree of Server_Node to
    build an application
    """

    IP=String(default="localhost")
    PORT=Integer(default=8000)
    DEBUG=Boolean(default=True)
    TEMPLATE_FOLDER=String(default=None)
    #-------------------------------------------------------
    def onSetup(self):
    #-------------------------------------------------------
        #print("#"*50,self.path())

        #self.setEnviron()
        #self.setCommandLine()
        #self.processTree()


        self.app = Flask(self.name.replace(".","_"),template_folder=self.template_folder)
        CORS(self.app)
        self.app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1

        self.methods=['GET',]

        for child in self.all().by_class(Api_Node):
            print(child.get_url())
            child.map(self.app)
        IoNode.onSetup(self)

    #-------------------------------------------------------
    def onPostSetup(self):
    #-------------------------------------------------------
        print("setup done")#self.tree()

    #-------------------------------------------------------
    def onDo(self):
    #-------------------------------------------------------

        self.app.run(host=self.ip,
                        port=int(self.port),
                        debug=self.debug, 
                        threaded=True, 
                        use_reloader=False)


    #-------------------------------------------------------
    def ask(self,**args):
    #-------------------------------------------------------
        return self.process_request(**args)

    #-------------------------------------------------------



#============================================================
