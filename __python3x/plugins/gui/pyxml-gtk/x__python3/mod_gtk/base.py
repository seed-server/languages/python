
from  mod_program import Code,Object
from dataModel import *
from xmlscript import tree_from_file

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gio

#====================================================

class Black_Event(Object):

#====================================================
    EVENT=String()

    #-------------------------------------------------
    def onMake(self):
        self.parent.gtk_object.connect(self.event,self.onEvent)

    #-------------------------------------------------
    def onEvent(self,*args):        
        print(self.event,args)
    #-------------------------------------------------
    
#====================================================

class Black_Item(Object):

#====================================================
    POSITION=String(default="start")#add,start,end in container
    FILL=Boolean(default=False)
    EXPAND=Boolean(default=False)
    PADDING=Integer(default=0)

    #-------------------------------------------------
    def onSetup(self):
        self.gtk_object=None
    
    #-------------------------------------------------
    def onDestroy(self):
        if self.gtk_object is not None:
            self.gtk_object.destroy()
            self.gtk_object=None
            
    #-------------------------------------------------
    def onPreMake(self,data):
        pass
    #-------------------------------------------------        
    def onPostMake(self,data):
        pass
    #-------------------------------------------------
    def make(self,data):
        print("MAKE",self.path())
        self.onPreMake(data)

        for elt in self.children.by_class(Black_Item):
            elt.make(data)
            
        for elt in self.children.by_class(Black_Event):
            elt.onMake()
        self.onPostMake(data)
        self.gtk_object.show_all()
        if self.parent is not None:
            self.parent.add(self)
    #-------------------------------------------------        
    def add(self,node):
        print("add",node.path())
        self.gtk_object.add(node.gtk_object)
    #-------------------------------------------------
#====================================================

class Black_Container(Black_Item):

#====================================================

    H=Boolean(default=False)

    #-------------------------------------------------
    def onPreMake(self,data):
        if self.h == True:
            self.gtk_object = Gtk.HBox()
        else:
            self.gtk_object = Gtk.VBox()
        #self.gtk_object.set_hexpand(True)
        #self.gtk_object.set_vexpand(True)
        
    #-------------------------------------------------        
    def onPostMake(self,data):
        pass
        
    #-------------------------------------------------        
    def add(self,node):
        print("add",node.path())
        self.gtk_object.add(node.gtk_object)
    #-------------------------------------------------        
    def add(self,node):

        if node.position == "start":
            self.gtk_object.pack_start(node.gtk_object,node.expand,node.fill,node.padding)
        elif node.position == "end":
            self.gtk_object.pack_end(node.gtk_object,node.expand,node.fill,node.padding)                
        elif node.position == "add":
            self.gtk_object.add(node.gtk_object)                

    #-------------------------------------------------

