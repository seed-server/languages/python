from  mod_program import Code,Object
from dataModel import *

import gi
print("gtk")
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from gi.repository.WebKit2 import WebView, Settings
from .base import *
from .editor import *
#======================================================

class Glade_Event(Object):

#======================================================

    def call(self,obj):
        print(self.name)

#======================================================

class Glade_Object(Black_Item):

#======================================================

    #-----------------------------------------------------     
    def make(self,data):
        self.gtk_object=self.get_object(self.name)
        if self.gtk_object is None:
            raise Exception("no object",self.name)

        print(self.gtk_object)
        self.onBuildGtk()
        for elt in self.children.by_class(Black_Item):
            elt.make(data)
    #-----------------------------------------------------         
    def onBuildGtk(self):
        pass
    #-----------------------------------------------------     
    def get_object(self,node_id):
        return self.get_builder().get_object(node_id)
    #-----------------------------------------------------     
    def get_builder(self):
        return self.parent.get_builder()
    #----------------------------------------------------- 

  
#======================================================

class Black_Browser(Glade_Object):

#======================================================

    URL=String()
    #-----------------------------------------------------     
    def onBuildGtk(self):
        self.gtk_object.load_uri(self.url)
    #-----------------------------------------------------   

#======================================================

class Glade_Main(Black_Editor):

#======================================================

    GLADE_FILE=String()
    WINDOW_ID=String()
    TITLE=String()
    
    #----------------------------------------------------- 
    def onSetup(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.glade_file)

    #----------------------------------------------------- 
    def onPreMake(self,data):
        self.gtk_object = self.builder.get_object(self.window_id)
        #self.gtk_object.set_title(self.title)
        self.gtk_object.connect('delete-event', Gtk.main_quit)
        
        events=dict()
        for event in self.all().by_class(Glade_Event):
            events[event.name]=event.call
        self.builder.connect_signals(events)
        
    #----------------------------------------------------- 
    def onPostMake(self,data):
        self.gtk_object.show_all()
        Gtk.main()
        
    #----------------------------------------------------- 
    def get_builder(self):
        return self.builder
    #----------------------------------------------------- 
#======================================================

