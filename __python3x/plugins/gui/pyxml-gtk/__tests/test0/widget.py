import gi
from gi.repository import GLib, Gio, Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
from settings import settings
from gi.repository.GdkPixbuf import Pixbuf
Notify.init (settings["title"])
from web import MyWeb

from icons import IconViewWindow

def send_notification(content,icon="dialog-information"):
    notif=Notify.Notification.new (settings["title"],content,icon)
    notif.show ()
    
@Gtk.Template(filename=settings["widgets"]+"/widget.ui")
class MyBox(Gtk.Box):
    __gtype_name__ = "MyBox"

    notif_btn = Gtk.Template.Child()
    stop_btn = Gtk.Template.Child()
    
    @Gtk.Template.Callback()
    def onButtonPressed1(self, button):
        send_notification("This is an example notification.",icon="dialog-information")
        self.box = MyWeb()
        self.add(self.box)    
        self.show_all()
        
    @Gtk.Template.Callback()
    def onButtonPressed(self, button):
        send_notification("http://nx.anadenn.fr",icon="emblem-important-symbolic")
        self.box = IconViewWindow()
        self.add(self.box)    
        self.show_all()
 
