       
from mod_program import *
from data_model import *

class Gtk_Widget(Object):

    GTK_CLASS=String()
    GTK_TEMPLATE=String()

    def onSetup(self):
        pass
        
    def onCleanup(self):
        pass
        
    def get_Gtk_class(self,name,template,CLS=Gtk.Box):

        @Gtk.Template(filename=settings["widgets"]+template)
        class Gtk_Base_Widget(CLS):
            __gtype_name__ = name
    
    return Gtk_Base_Widget
    

