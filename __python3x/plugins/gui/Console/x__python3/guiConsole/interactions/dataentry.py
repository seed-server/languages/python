# -*- coding: utf-8 -*-
from .model import Dialog
#http://www.siteduzero.com/tutoriel-3-460602-gestion-des-mots-de-passe.html
from getpass import getpass
#==========================================================================

class Dialog_Input(Dialog):

#==========================================================================

    
    def ask(self,node):
        return raw_input(self.title+" =")

  

#==========================================================================

class Dialog_Password(Dialog_Input):
    
#==========================================================================


    def ask(self,node):

        return getpass(self.title+" =")

#==========================================================================

class Dialog_String(Dialog_Input):

#==========================================================================

    def ask(self,node):
        return unicode(raw_input(self.title+" ="))

#==========================================================================

class Dialog_Integer(Dialog_Input):
    
#==========================================================================


    def ask(self,node):
        return int(raw_input(self.title+" ="))


#==========================================================================

class Dialog__Boolean(Dialog_Input):

#==========================================================================


    #def ask(self,text="yes/no",yestag="yes",notag="no",**args):
    def ask(self,node):
        result=None
        result = raw_input(self.text)
        if result == yestag:
            result=True
        elif result == notag:
            result=False
        else:
            result=None
            
        return result
#==========================================================================

