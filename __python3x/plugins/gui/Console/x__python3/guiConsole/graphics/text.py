from .areas import Console_Square
from dataModel import *
#========================================================================

class Console_Text(Console_Square):

#========================================================================
    TEXT=String()
    UPPER=Boolean(default=False)

    def fill_matrix(self,matrix,**args):

        if self.upper==True:
            text=self.text.upper()
        else:
            text=self.text

        if self.parent.border is None:
            size=self.parent.lx
            offset=0
        else:
            size=self.parent.lx-2
            offset=1

        lines=0
        for txt in self.split_text(text,size):

            if lines < (self.parent.ly):
                break
            else:
                #print("A",txt)
                matrix.setStringLine(lines,txt)
                lines+=1
        #print("X")
        matrix.show()

    def split_text(self,txt,l):

        if len(txt) < l:
            yield txt

        else:
            i=0
            j=l
            while j < len(txt):
                yield txt[i:j]
                i=j+1
                j=i+l

            if j < len(txt):
                yield txt[j:]


#========================================================================



