from .areas import Console_Square
from dataModel import *
#========================================================================

class Console_VFrame(Console_Square):

#========================================================================
    I=Integer(default=0)    
    MSGS=List()

    def fill_matrix(self,matrix,**args):

        i=1
        for area in self.children.by_class(Console_Square):
            sub_matrix=area.get_matrix()
            matrix.add_matrix(sub_matrix,(area.x,area.y))



    def add_element(self,elt):


        self.msgs.append(elt)

        elt.parent=self
        elt.x=self.i
        elt.y=1
        if elt.lx in [None,0]:elt.lx=1
        elt.ly=self.ly
        self.i+=elt.lx
        #print(elt,elt.parent)

#========================================================================

class Console_VScroll(Console_VFrame):

#========================================================================

    def fill_matrix(self,matrix,**args):

        #print(self.i , self.lx)

        if self.i > self.lx:

            while self.i > self.lx:
                #print(self.i , self.lx,len(self.msgs))
                to_destroy=self.msgs[0]
                self.i-=to_destroy.lx
                self.msgs.remove(to_destroy)
                to_destroy.destroy()

            i=1
            for msg in self.msgs:

                msg.x=i
                i+=msg.lx


        Console_VFrame.fill_matrix(self,matrix,**args)


#========================================================================
