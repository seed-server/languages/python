from .areas import Console_Square
from dataModel import *
#========================================================================

class Console_Fullscreen(Console_Square):

#========================================================================

    def onSetup(self,**args):
        import shutil
        ly, lx = shutil.get_terminal_size(fallback=(80, 24))

        self.x,self.y=(0,0)
        self.lx,self.ly=(int(lx)-1,int(ly))
        return Console_Square.onSetup(self)


#========================================================================
