from dataModelAddons.tree.structure import Tree_Builder
from .parser import TimeParser

#==============================================================

class TimeTree(Tree_Builder):

#==============================================================

    #---------------------------------------------------------

    def __init__(self,precision="FULL",**args):

        parser=TimeParser(precision=precision)
        Tree_Builder.__init__(self,parser=parser,**args)

    #---------------------------------------------------------

    def now(self,**args):

        t=self.parser.now()
        node = self[t]
        args["time"]=t
        node.update(args)
        return node

    #---------------------------------------------------------

#==============================================================
