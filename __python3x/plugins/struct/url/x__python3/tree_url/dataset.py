"""
l'arbre d'url servira d'identifiant unique pour les données
les noeuds de l'arbre seront typés et utilisables directement
pour interagir avec les données
"""
from urllib.parse import urlparse,parse_qs
import os,sys

from atom import Atom,Atom_Link
from dataModel import *

from mod_program import *
DEFAULT_SCHEME="files"
DEFAULT_USER="anonymous"
DEFAULT_HOST="localhost"
#===============================================================

class Urls_Node(Data):
    
#===============================================================
    """
    classe abstraite pour les urls
    """
    def onSetup(self):
        pass
    #--------------------------------------------------------------
    def get_domain(self):
        return self.parent.get_domain()

    #--------------------------------------------------------------
    def get_scheme(self):
        return self.parent.get_scheme()

#===============================================================

class Urls_Query(Urls_Node):
    
#===============================================================
    def onSetup(self):
        pass
    
#===============================================================

class Urls_Fragment(Urls_Node):
    
#===============================================================
    GET_QUERY=Function()
    def onSetup(self):
        pass
    #--------------------------------------------------------------
    def get_query(self,name=None):
        return self.append(name,cls=Urls_Query)
    #--------------------------------------------------------------

#===============================================================

class Urls_Path(Urls_Node):
    
#===============================================================
    GET_FRAGMENT=Function()
    GET_QUERY=Function()
    #--------------------------------------------------------------
    def onSetup(self):
        pass
    #--------------------------------------------------------------
    def get_fragment(self,name=None):
        return self.append(name,cls=Urls_Fragment)

    #--------------------------------------------------------------
    def get_query(self,**args):
        return Urls_Query(parent=self,**args)
    #--------------------------------------------------------------

#===============================================================

class Urls_User(Urls_Path):
    
#===============================================================
    GET_PATH=Function()
    def onSetup(self):
        pass
    #--------------------------------------------------------------
    def get_path(self,path=None):
        return self.append(path,cls=Urls_Path)
    #--------------------------------------------------------------
#===============================================================

class Urls_Scheme(Urls_Node):
    
#===============================================================
    GET_USER=Function()
    def onSetup(self):
        pass

    #--------------------------------------------------------------
    def get_user(self,username=None,password=None):
        return self.append(username,cls=Urls_User,password=password)

    #--------------------------------------------------------------
    def get_scheme(self):
        return self

#===============================================================

class Urls_Domain(Urls_Node):
    
#===============================================================
    GET_SCHEME=Function()
    def onSetup(self):
        pass
    #--------------------------------------------------------------
    def get_domain(self):
        return self

    #--------------------------------------------------------------
    def get_scheme(self,scheme=None,port=None):

        if port is None:
            name=scheme
        else:
            name=scheme+"_"+str(port)

        return self.append(name,cls=Urls_Scheme,port=port,scheme=scheme)

    #--------------------------------------------------------------

#===============================================================

class Urls_Tree(Urls_Node):
    
#===============================================================

    GET_FROM_URL=Function()
    GET_DOMAIN=Function()
    #--------------------------------------------------------------
    def onSetup(self):
        pass


    #--------------------------------------------------------------
    def get_from_url(self,url):
        "return a node from url"
        data= urlparse(url)
        return self.__get_node(self,data,url=url)

    #--------------------------------------------------------------
    def get_domain(self,domain=None):
        return self.append(domain,cls=Urls_Domain,separator="/",reverse=True)
    #--------------------------------------------------------------
    def __get_node(self,root,data,**args):

        if data.scheme  in ["",None]:
            scheme=DEFAULT_SCHEME
        else:
            scheme=data.scheme

        if data.username in ["",None]:
            username=DEFAULT_USER
        else:
            username=data.username

        if data.hostname  in ["",None]:
            hostname=DEFAULT_HOST
        else:
            hostname=data.hostname
    

        domain=root.get_domain(hostname)
        scheme=domain.get_scheme(scheme=scheme,port=data.port)
        node=scheme.get_user(username=username,password=data.password)

        if data.path =="":
            node=node.get_path(path="/")
        else:
            node=node.get_path(path=data.path)

        if data.fragment !="":
            node=node.get_fragment(data.fragment)


        if data.query !="":
            node=node.get_query(**parse_qs(data.query))
            #node.show()

        node.update(**args)
        return node
    #--------------------------------------------------------------


#=======================================================================

