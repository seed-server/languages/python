#model:Class.py
"""
module
---------------------------------------------------

description

"""
from atom import Atom,Atom_Link
from dataModel import *
import math
from  math import *
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from mod_program import Object,Data

#=====================================================================

class Spatial_Node(Data):

#=====================================================================

    X=Float(default=0.0)
    Y=Float(default=0.0)
    Z=Float(default=0.0)

#=====================================================================

class Planet_Node(Data):

#=====================================================================
    def get_layer(self):
        for elt in self.ancestors:
            if isinstance(elt,Planet_Layer):
                return elt

    def is_inside(self,node):
        pass


    def plot(self):
        X=list()
        Y=list()
        for elt in self.children.by_class(Planet_Place):
            x,y=elt.local_coords(self)
            X.append(x)
            Y.append(y)
            #print(elt.name,x,y)

        plt.scatter(X,Y)
        plt.show()

    def plot3D(self):
        X=list()
        Y=list()
        Z=list()

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')


        # Plot a basic wireframe.
        ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)

        plt.show()


#=====================================================================

class Planet_Place(Planet_Node):

#=====================================================================
    "distances en metres"

    LON=Float(default=0.0)
    LAT=Float(default=0.0)
    Z=Float(default=0.0)
    R=Float(default=10)
    DISTANCE=Function()
    PLOT=Function()

    def distance(self,node):

        return self.distance_from_coord(node.lon,node.lat)

    def local_coords(self,ref):

        lon_s=1
        lat_s=1
        if self.lon-ref.lon<0:
            lon_s=-1
        if self.lat-ref.lat<0:
            lat_s=-1

        return lon_s*self.distance_from_coord(ref.lon,self.lat),lat_s*self.distance_from_coord(self.lon,ref.lat)

    def is_inside(self,node):
        return self.distance(node) <= self.r

    def distance_from_coord(self,lon,lat):

        radius = self.get_layer().radius

        dLat = (lat-self.lat) * math.pi / 180
        dLng = (lon-self.lon) * math.pi / 180

        lat1 = self.lat * math.pi / 180
        lat2 = lat * math.pi / 180

        val = sin(dLat/2) * sin(dLat/2) + sin(dLng/2) * sin(dLng/2) * cos(lat1) * cos(lat2)    
        ang = 2 * atan2(sqrt(val), sqrt(1-val))
        return radius * ang


#=====================================================================

class Planet_Line(Atom_Link):

#=====================================================================

    def distance(self):
        return self.source.distance(self.target)

#=====================================================================

class Planet_Polygon(Planet_Node):

#=====================================================================
    CLOSED=Boolean(default=False)
    DISTANCE=Function()


    def distance(self):
        result=0
        ok=True
        i=1
        lst=self.children.by_class(Planet_Place)
        l=len(lst)
        if l>1:
            node=lst[0]
            while ok:
                result+=lst[i].distance(node)
                node=lst[i]
                i+=1
                if i ==l:
                    ok=False

            if self.closed==True:
                result+=lst[-1].distance(lst[0])
        return result


#=====================================================================

class Planet_Sector(Planet_Place):

#=====================================================================
    MIN_LON=Float()
    MIN_LAT=Float()
    MAX_LON=Float()
    MAX_LAT=Float()

    def onSetup(self):
        self.lon=self.min_lon+(self.max_lon-self.min_lon)/2.0
        self.lat=self.min_lat+(self.max_lat-self.min_lat)/2.0

    def is_inside(self,node):
        return ( (node.lon >= self.min_lon) 
                 and (node.lon <= self.max_lon) 
                 and (node.lat >= self.min_lat) 
                 and (node.lat <= self.max_lat) )

        

#=====================================================================

class Planet_Grid(Planet_Place):

#=====================================================================
    N=Integer(default=24)
    M=Integer(default=11)

    def onSetup(self):

        lon_angle=360/self.n
        lat_angle=180/self.m
        for i in range(self.n):
            lon=-180+lon_angle*i
            for j in range(self.m):
                lat=-90+lat_angle*j
                #print(i,j,lon,lat)
                Planet_Sector(parent=self,name=str(i)+"_"+str(j),min_lon=lon,
                                            max_lon=lon+lon_angle,
                                            min_lat=lat,
                                            max_lat=lat+lat_angle,i=i,j=j)



#=====================================================================

class Planet_Layer(Planet_Place):

#=====================================================================
    #return distance as meter if you want km distance, remove "* 1000"
    RADIUS=Float(default=6371)# * 1000 )


#=====================================================================

class Earth(Planet_Layer):

#=====================================================================

    def onSetup(self):
        Planet_Place(parent=self,name="zero",lon=0.0,lat=0.0)
        Planet_Place(parent=self,name="north",lon=0.0,lat=90.0)
        Planet_Place(parent=self,name="south",lon=0.0,lat=-90.0)
        Planet_Place(parent=self,name="east",lon=180.0,lat=0.0)
        Planet_Place(parent=self,name="west",lon=-180.0,lat=0.0)
#=====================================================================
