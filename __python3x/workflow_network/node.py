# -*- coding: utf-8 -*

from mod_program import Iterator
#from .relations import Links_NeighborList,Links_SubjectList,Links_ObjectList
#from .relations import Links_Subjects,Links_Objects,Links_Neighbors


#===============================================================================

class Node_Iterator(Iterator):
    
#===============================================================================

    #RefClass=Tree_Node

    def onIter(self,node):
        pass

#===============================================================================

class Inputs(Node_Iterator):
    
#===============================================================================

    def onIter(self,node):
        for elt in node.inputs:
            yield elt


#===============================================================================

class Outputs(Node_Iterator):
    
#===============================================================================

    def onIter(self,node):
        for elt in node.outputs:
            yield elt

#===============================================================================

class Subjects(Node_Iterator):
    
#===============================================================================

    def onIter(self,node):
        for elt in node.inputs:
            yield elt.source


#===============================================================================
class Objects(Node_Iterator):
#===============================================================================

    def onIter(self,node):
        for elt in node.outputs:
            yield elt.target

#===============================================================================
class Neighbors(Node_Iterator):
#===============================================================================

    def onIter(self,node):
        done=[]
        for link in node.outputs:

            if link.target not in done:
                yield link.target
                done.append(link.target)

        for link in node.inputs:
            if link.source not in done:
                yield link.source
                done.append(link.source)
        del done

#===============================================================================

