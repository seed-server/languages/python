#model:Class.py
"""
module
---------------------------------------------------

description

"""
from atom import *
from dataModel import *
from .cell import *
from .nervousSystem import *
#=====================================================================

class Send_Order(Adn_Base):

#=====================================================================

    #----------------------------------------------------------------
    def onCall(self,message):
        brain=None
        for elt in self.ancestors:
            if isinstance(elt,Brain)==True:
                brain=elt
                break
        if brain:
            message=Nerve_Order(parent=brain,name=self.name)
            message.execute(brain)

#=====================================================================

class Message_Show(Adn_Base):

#=====================================================================

    #-----------------------------------------------------------------
    def onSetup(self):
        pass

    #-----------------------------------------------------------------
    def onCleanup(self):
        pass
    #----------------------------------------------------------------
    def onCall(self,message):
        #print("Message",message.text)
        message.show()

    #-----------------------------------------------------------------

#=====================================================================
