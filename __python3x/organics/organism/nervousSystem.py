#model:Class.py
"""
module
---------------------------------------------------

description

"""
from atom import *
from dataModel import *
from IoNode import *
import time
from .Organ import *
from .cell import *
#=====================================================================

class Nerve_Order(Body_Message):

#=====================================================================
    """
    transporte l'information
    """

    #-----------------------------------------------------------------
    def onFinish(self,message,node):

        for pipe in node.outputs.by_class(Nerve_Pipe):
            pipe.send_message(message)

#=====================================================================

class Nerve_Info(Body_Message):

#=====================================================================
    """
    transporte l'information
    """
    #-----------------------------------------------------------------
    def onFinishX(self,node):
        send=False
        for pipe in node.inputs.by_class(Nerve_Pipe):
            pipe.source.send(message=self)
            send=True

        if send == False:
            self.parent=node
            self.stop()

#=====================================================================

class Nerve_Pipe(Body_Pipe):

#=====================================================================

    CLASS=Nerve_Order

#=====================================================================

class Nerve_Receptor(Cell):

#=====================================================================
    def onStart(self):
        node=Nerve_Info(parent=self)
        node.start()
        node.execute(self)
        node.stop()

    def onDo(self):
        pass

#=====================================================================

class Nerve(Cell):

#=====================================================================
    CLS=String()

    def onPostSetup(self):
        Nerve_Pipe(parent=self,source=self,target=self.node)

        for elt in self.node.children.by_class(Cell):
            Nerve_Pipe(parent=self,source=self,target=elt)

    #-----------------------------------------------------------------

#=====================================================================

class Brain(Organ):

#=====================================================================
    def onPostSetup(self):
        Nerve_Pipe(parent=self,source=self,target=self.parent)
        for elt in self.parent.children.by_class(Organ):
            if elt !=self:
                node=Nerve(parent=self,name=elt.name,node=elt)
                node.setup()
                Nerve_Pipe(parent=self,source=self,target=node)

    #-----------------------------------------------------------------


#=====================================================================


