from .cell import *
from .Organ import *

#=====================================================================

class Globule(Body_Message):

#=====================================================================
    """
    transporte l'information
    """


    #-----------------------------------------------------------------
    def onFinishX(self,node):
        for pipe in node.outputs.by_class(Heart_Pipe):
            pipe.target.send(message=self)



 #=====================================================================

class HeartX(Organ):

#=====================================================================

    TAG=String(default="blood")
    DT=Float(default=1.0)

    def onPostSetup(self):
        if self.parent is not None:

            for organ in self.parent.children.by_class(Organ):
                if organ !=self:
                    for elt in organ.children.by_class(Organ_Input):
                        Relation(parent=self,source=self,target=elt,tag=self.tag)

                    for elt in organ.children.by_class(Organ_Output):
                        Relation(parent=self,source=elt,target=self,tag=self.tag)

        self.i=0
        self.t0=time.time()

    #----------------------------------------------------------------

    def onStart(self):
        pass        
    #-----------------------------------------------------------------
    def onReceiveMessage(self,message):
        if isinstance(message,Body_Message):
            #message.execute(self)
            #print("MESSAGE",message.path())
            pass#message.stop()

    #----------------------------------------------------------------

    def onDo(self):

        if time.time()<self.t0+self.dt:
            return 

        message=self.new_message(cls=Globule,name=str(self.i),tag=self.tag)

        self.i+=1
        self.t0=time.time()
        #self.tree()

    #----------------------------------------------------------------

    def onStop(self):
        pass

    #-----------------------------------------------------------------

#=====================================================================
