from atom import *
from dataModel import *
from IoNode import *
import time

from ..entity import *
from .Organ import *

#=====================================================================

class Organism(Entity):

#=====================================================================

    #----------------------------------------------------------------

    def report(self,name):
        print(name.upper(),"/",self.is_running())
        for elt in self.children.by_class(Organ):
            print(name.upper(),elt.path(),elt.is_running())
    #----------------------------------------------------------------

    def onSetup(self):
        for elt in self.children.by_class(Organ):
            Relation(parent=self,source=self,target=elt)
        Entity.onSetup(self)
    #----------------------------------------------------------------

    def onStart(self):
        self.report("start")
        #self.show()

    #----------------------------------------------------------------

    def onDo(self):

        for organ in self.children.by_class(Organ):
            for elt in organ.children.by_class(Body_Message):

                if elt.is_done()==True:
                    elt.source=elt.parent.path()
                    elt.parent=None
                    self.send_to_pipe(message=elt)
                    return

    #----------------------------------------------------------------

    def onStop(self):
        self.report("stop")
    #----------------------------------------------------------------

#=====================================================================
