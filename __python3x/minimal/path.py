
import os


#======================================================================

def FileListContainer(funct):

#======================================================================
    """
    * decorateur pour les fonctions
    * retourne une liste de chemin FilePath dans une liste FileList
    """
    def decorator(*args,**kwargs):

        return FilePathList(funct(*args,**kwargs))
    return decorator

#======================================================================

def FilePathContainer(funct):

#======================================================================
    """
    * decorateur pour les fonctions
    * construit un objet FilePath avec le chemin retourné par la fonction
    """
    def decorator(*args,**kwargs):

        return FilePath(funct(*args,**kwargs))
    return decorator

#======================================================================
@FileListContainer
def ls(path,recursive=False,hidden=False,**args):

#======================================================================
    """
    * renvoie la liste de tous les fichiers dans un objet FileList
    * options :
       * recursive : si True recursif, sinon repertoire courant, par défaut False
       * hidden : si True inclue les fichiers cachés, par défaut False
    """

    path=FilePath(path)

    try:
        result = [path.join(elt) for elt in os.listdir(path.string)]
    except:
        return


    if hidden==False:

        for elt in list(result):
            if elt.name().startswith("."):
                result.remove(elt)

    if recursive==True:

        for elt in list(result):
            if elt.isdir():
                result.extend(ls(elt,recursive=True,hidden=hidden))


    return result



#======================================================================
@FileListContainer
def filterfiles(path,*extentions,**args):

#======================================================================
    """
    * filtre les extentions
    * renvoie la liste de tous les fichiers dans un objet FileList
    * options :
       * recursive : si True recursif, sinon repertoire courant, par défaut False
       * hidden : si True inclue les fichiers cachés, par défaut False
    """

    global ls

    for elt in ls(path,**args).files():
        #print(elt.extention(),extentions)
        if elt.extention().lower() in extentions:

            yield elt

#======================================================================
@FileListContainer
def filterdirectories(path,initfile,**args):

#======================================================================
    """
    * renvoie la liste de tous les répertoires dans un objet FileList
    * options :
       * recursive : si True recursif, sinon repertoire courant, par défaut False
       * hidden : si True inclue les fichiers cachés, par défaut False
    """

    global ls

    for elt in ls(path,**args).directories():
        if elt.join(initfile).exists():
            yield elt

#========================================================================

class FilePathList(list):

#========================================================================
    """
    liste de chemins
    """


    def __init__(self,lst=None):
        if lst is not None:
            self.extend(lst)


    @FileListContainer
    def sort(self):

        lst=list()
        for elt in self:
            lst.append((elt.name(),elt))
        lst.sort()
        for elt in lst:
            yield elt[1]   

    def names(self,**args):

        for elt in self:
            yield FilePath(elt).name(**args)


    def relative(self,start,**args):

        start=FilePath(start)

        for elt in self:
            yield FilePath(elt).relative(start.string,**args)


    @FileListContainer
    def directories(self):

        for elt in self:
            if elt.isdir():
                yield elt

    @FileListContainer
    def files(self):

        for elt in self:
            if elt.isfile():
                yield elt


    def extentions(self):
        done=[]
        for elt in self:
            ext = FilePath(elt).extention()
            if ext not in done:
                done.append(ext)
        del done

    def filterdirectories(self,initfile):

        for elt in self:
            if elt.join(initfile).exists()==True:
                yield elt

    @FileListContainer
    def filterfiles(self,*extentions):
        for elt in self:
            #print(elt.extention(),extentions)
            if elt.extention().lower() in extentions:
                yield elt

    def filtername(self,startswith=None,contains=None):
        for elt in self:
            if startwith and elt.name().startswith(startswith):
                yield elt
            elif contains and contains in elt.name():
                yield elt

    @FileListContainer
    def filtersystem(self):

        for elt in self:

            if elt.name().startswith("__")!=True:
                yield elt

#========================================================================

class FilePath:

#========================================================================

    #----------------------------------------------------------------

    def __init__(self,path):

        #if path is given
        if path is None:
            path="."

        if type(path) == str:
            self.string=path#os.path.abspath(path)

        elif isinstance(path,FilePath):
            self.string=path.string

        else:
            raise Exception("type incorrect : %s = %s"%(str(type(path)),str(path)))


        #self.string=unicode(self.string)
        #self.string=self.string.decode('utf-8')
        #self.string=self.string.encode('utf-8')

        #http://www.developpez.net/forums/d1148561/autres-langages/python-zope/general-python/ouvrir-repertoire-accent-path/
        #self.string=self.string.decode('utf-8')#.encode('cp1252')
    #----------------------------------------------------------------

    def __str__(self):

        return self.string

    #----------------------------------------------------------------

    def split(self):

        path,name=os.path.split(self.string)
        name,ext=os.path.splitext(name)
        ext=ext[1:]#retirer le point
        return path,name,ext   

    #----------------------------------------------------------------

    def name(self,extention=True):

        name=os.path.split(self.string)[1]
        if extention==True:
            return name
        else:
            return os.path.splitext(name)[0]   

    #----------------------------------------------------------------

    @FilePathContainer
    def root(self):
        return os.path.split(self.string)[0]


    #----------------------------------------------------------------

    def extention(self):
        ext=os.path.splitext(self.string)[1]#recuperer l'extention
        ext=ext[1:]#retirer le point
        return ext
        
    #----------------------------------------------------------------

    def exists(self):

        return os.path.exists(self.string)

    #----------------------------------------------------------------
    @FilePathContainer
    def join(self,*args):

        return os.path.join(self.string,*args)

    #----------------------------------------------------------------
    def make_dirs(self,*args):

        if self.exists()==False:

            if "." in self.name():

                self.root().make_dirs()

            else:

                os.makedirs(self.string)

                

    #----------------------------------------------------------------
    def isfile(self,*args):

        return os.path.isfile(self.string)


    #----------------------------------------------------------------
    def isdir(self,*args):

        return os.path.isdir(self.string)

    #----------------------------------------------------------------
    def islink(self):

        return os.path.islink(self.string)
    #----------------------------------------------------------------
    def hidden(self):

        return self.name().startswith(".")

    #----------------------------------------------------------------
    def  relative(self,start):

        return os.path.relpath(self.string, str(start))
    #----------------------------------------------------------------
    def  cut(self,path):

        return self.string.replace(os.path.abspath(path)+"/","")

    #----------------------------------------------------------------

    def infos(self):
        """
        renvoie les infos de la fonction os.stat


        - st_mode - protection bits,
        - st_ino - inode number,
        - st_dev - device,
        - st_nlink - number of hard links,
        - st_uid - user id of owner,
        - st_gid - group id of owner,
        - st_size - size of file, in bytes,
        - st_atime - time of most recent access,
        - st_mtime - time of most recent content modification,
        - st_ctime - platform dependent; time of most recent metadata change on Unix, or the time of creation on Windows)


        https://docs.python.org/2/library/os.html
        """

        headers=("mode", "ino", "dev", "nlink", "uid", "gid", "size", "atime", "mtime", "ctime")
        infos=os.stat(self.string)
        infos=dict(zip(headers,infos))
        infos["path"]=self.string
        return infos

#========================================================================

