



#===============================================================================

class Parser(object):

#===============================================================================


    #-----------------------------------------------------------  

    def init(self,parent=None,Class=None,pathkey="name",**args):

        self.parent=parent
        self.Class=Class
        self.pathkey=pathkey

    #-----------------------------------------------------------

    def getElement(self,path,build=False,data=None):

        node=self.parent

        if path in ["/",""]:
            return node


        lst=list(self.parseKey(path))
        l=len(lst)
        i=0

        for pathElement,fullPath in lst:
            i+=1
            found=False

            for child in node.children:

                child_pathkey=child[self.pathkey]
                if child_pathkey == pathElement:
                    node=child
                    found=True
                    if i == l:
                        if data is not None:
                            node.update(**data)
                    break

            if not found:

                if build==True:

                    if i < l:
                        node_args=dict()
                    elif data is None:
                        node_args=dict()
                    else:
                        node_args=data                    
                    node_args[self.pathkey]=pathElement
                    node=self.Class(parent=node,**node_args)
                else:
                    raise Exception("Element not found",path)

        return node

    #-----------------------------------------------------------

    def parseKey(self,path):
        full=""
        for elt in path:
            full+=elt
            yield elt,full

    #-----------------------------------------------------------

    def getKey(self,elt):
        return elt["__path__"]

    #-----------------------------------------------------------

    def to_list(self,path):

        for elt,elt2 in self.parseKey(path):
            yield elt

#===============================================================================

