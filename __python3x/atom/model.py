import os,traceback,glob
from dataModel import *
from .variables import VariableManager


#==========================================================

class Atom(Model,Tree,Network_Node,VariableManager,Tree_Initializer,Manage_Query,Manage_Help):
    
#==========================================================

    J=0
    #SETUP   =Function(TreeLogs())
    #CLEANUP =Function(TreeLogs())
    #OPEN_FILE =Function()#TreeLogs(),filename=String())
    #NEW_NODE =Function(TreeLogs())
    #ADD_FILE =Function(TreeLogs())
    #MAKE_LINK =Function()#TreeLogs())

    #----------------------------------------------------------------

    def __str__(self):

        return "/"+self.path()
    #---------------------------------------------------

    def get_infos(self):
        """
        renvoie un dictionnaire avec les attributs de l'élément
        enregistrables. Pas de Frame,objets etc...

        en ajoutant le chemin (accessible avec la clef 'path')

        utile dans la fonction process_string
        """
        d=dict(self.getRecordableDict())
        d["path"]=self.path()
        return d
    #---------------------------------------------------

    def process_string(self,string,**args):
        """
        remplacer les argument ans la chaine de caractères

        * crée un dictionnaire avec les attributs de l'élément
        * met à jour avec les arguments de la fonction
        * remplace dans la chaine de caractères

        les arguments de la fonction écrasent ceux du noeud
        utile pour créer des urls, des titres, etc...
        """
        #print(string%self.get_infos())
        d=self.get_infos()
        d.update(args)
        return string%d
    
   #---------------------------------------------------

    def onInit(self):

        self.autoname()
 
   #---------------------------------------------------
    def onPreSetup(self):

        self.processTree()

    #---------------------------------------------------


    def onSetup(self):
        "overload to have a behavior"
        pass
    
    #---------------------------------------------------
    
    def onCleanup(self):
        "overload to have a behavior"
        pass
    
   #---------------------------------------------------

    def query(self,action=None,**args):

        #print(action,args)
        if hasattr(self,action):
            r= getattr(self,action)(**args)
            if isinstance(r,getClass("Log_Message")):
                #r.parent=self
                #print(Atom.J)
                Atom.J+=1
                r.name=action+"_"+str(Atom.J)
            
            return r
        return "error no action "+action

    #---------------------------------------------------
    def log(self,**args):
        cls=getClass("Log_Message")
        cls(parent=self,**args)
    #-------------------------------------------------------------
    def get_title(self):

        string=self.name.replace("."," ")
        string=string.replace("_"," ")
        string=string.replace("-"," ")
        return string
    #-------------------------------------------------------------
    def append(self,path,cls=None,**args):
        if cls is None:
            cls=Atom
        node=Tree.append(self,path,cls=cls,**args)
 
        return node
    #-------------------------------------------------------------
    def getPath(self,*args):
        
        return self.file_path
        path=None
        
        #is attribut
        if hasattr(self,"file_path") == True and self.file_path is not None:
            
            path= self.file_path
            #si relatif
            if path.startswith("../"):
                path=self.parent.getPath(path[3:])
            #print(path)
        #sinon parent
        elif self.parent is not None:
            path= self.parent.getPath()
            
        #ajouter elements
        if path:
            path=os.path.join(path,*args)
            
        return path

    #-------------------------------------------------------------
    def get_parent(self,path):

        lst=path.split("/")

        if len(lst)>1:
            #print("search parent",lst)
            name=lst[-1]
            root="/".join(lst[:-1])
            if path.startswith("/"):
                parent=self.append("/"+root,cls=Atom)
            else:
                 parent=self.append(root,cls=Atom)

        else:
            name=path
            parent=self

        return name,parent

    #-------------------------------------------------------------
    def from_xml_dir(self,path=None,**args):
        from xmlscript import tree_from_file
        for elt in glob.glob(path+'/*.xml'):
            #print(elt)
            node=tree_from_file(elt,**args)

    #-------------------------------------------------------------
    def from_xml_file(self,filename=None,stream=None,setup=True,destination="",**args):

        print(filename)
        name,parent=self.get_parent(destination)
        #print(name,parent)
        #parent.tree()
        #print(name,parent.find(name,error=False))
        if parent.find(name,error=False) is None:

            if filename is not None:

                from xmlscript import tree_from_file
                file_path="/".join(filename.split("/")[:-1])
                node=tree_from_file(filename,file_path=file_path,name=name,parent=parent,**args)

            else:

                from xmlscript import tree_from_string
                node=tree_from_string(stream,name=name,parent=parent,**args)

            #node.tree()
            if  setup :
                self.doSetup(node)
            return node.path()

    #-------------------------------------------------------------
    def to_xml(self):

        from xmlscript import tree_to_string
        return tree_to_string(self)

    #-------------------------------------------------------------
    def to_xm_file(self,filename):
        from xmlscript import tree_to_file
        tree_to_file(filename,self)

    #-------------------------------------------------------------
    def set_attr(self,key=None,value=None):
        self[key]=value

    #-------------------------------------------------------------
    def get_attr(self,key=None):
        return str(self[key])

    
    #-------------------------------------------------------------
    def from_cls(self,destination=None,cls=None,setup=True,**args):

        if destination is not None:
            name,parent=self.get_parent(destination)
        else:
            name,parent=None,self

        #print(name)

        if cls is None:
            cls=Atom
        else:
            cls=getClass(cls)

        if parent is not None and parent.find(name,error=False) is not None:
            return "already exists :",destination

        elif name is not None:

            node=cls(parent=parent,name=name,**args)

        else:
            node=cls(parent=parent,**args)

        if  setup :
            node.query("setup")
        return node.path()

    #---------------------------------------------------

    def make_link(self,root=None,name=None,source=None,target=None,**args):

        #print("link",source,target)
        if type(root)==str:
            parent=self.append(root,cls=Atom)
        elif parent is None:
            parent=self
        
        return Atom_Link(name=name,source=source,target=target,parent=parent,**args).setup()



    #-------------------------------------------------------------
    def show_messages(self):
        for elt in self.children.by_class(cls="Log_Message"):
            print("   *",elt.text)

    #---------------------------------------------------



#==================================================================

class Atom_Link(Atom,Link):
    
#==========================================================



    def onSetup(self):
        #print(self.source,type(self.source))
        if type(self.source) == str:
            self.source= self.find(self.source)


        if type(self.target) == str:
            self.target= self.find(self.target)


#==========================================================

