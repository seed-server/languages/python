
from dataModel import *
from dataModelAddons import *
from .interfaces import TreeRunner



#===============================================================

class Link(Model,Tree,Network_Node,Link):
    
#===============================================================

    def onSetup(self):
        #print(self.source,type(self.source))
        if type(self.source) == str:
            self.source= self.find(self.source)

        if type(self.target) == str:
            self.target= self.find(self.target)

#===============================================================

class Node(Model,Tree,Network_Node):
    
#===============================================================

    def onSetup(self):
        pass
