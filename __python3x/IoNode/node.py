import time,threading
from queue import Queue
from dataModel import *
from mod_program import *
from dataModelAddons import TreeLogs,treelogs,Log_Message
from mod_program import Relation
from xmlscript import tree_from_string,tree_to_string
import time,threading


from .decorators import logs,to_output


#===================================================================

class IoNode_Interface(metaclass=ModelMeta):

#===================================================================

    def get_ionode(self):

        for elt in self.ancestors:
            if isinstance(elt,IoNode)==True:
                return elt

#===================================================================

class IoNode_Action(Action,IoNode_Interface):

#===================================================================

    pass

#===================================================================

class Exit(IoNode_Action):

#===================================================================

    def get_node(self):
        return self.get_ionode()

    def onNextCall(self,node):
        return node.stop()

#===================================================================

class Pause(IoNode_Action):

#===================================================================

    def get_node(self):
        return self.get_ionode()

    def onNextCall(self,node):
        return node.pause()
#===================================================================

class SendText(IoNode_Action):

#===================================================================
    TAG=String(default=None)
    TEXT=String(default="")

    def get_node(self):
        return self.get_ionode()

    def onNextCall(self,node):

        message=self.text#%node
        if node:
            node.send_to_pipe(tag=self.tag,text=message)
            return 


#===================================================================

class Send(IoNode_Action):

#===================================================================
    
    TAG=String(default=None)

    def onNextCall(self,node):
        ionode=self.get_ionode()
        if ionode:
            ionode.send_to_pipe(tag=self.tag,message=node)
            return 
        

#===================================================================

class Messages(Iterator,IoNode_Interface):

#===================================================================
    def get_node(self):
        return self.get_ionode()

    def onTestNode(self,node):
        return True

    def onIter(self,node):
        for msg in node.messages():
            yield msg

#===================================================================

class Pipe(Relation):

#===================================================================

    TAG=String(default=None)

    def onSetup(self):

        return Atom_Link.onSetup(self)

    #----------------------------------------------------------------
    #Messages
    #----------------------------------------------------------------
 
    def send_message(self,message):
        "add a query to the query box"

        if "tag" in message.keys() and message.tag is not None and  self.tag is not None:
            if message.tag != self.tag:
                return
        self.target.send(message=message)

    #----------------------------------------------------------------


#===================================================================

class IoNode(Code):

#===================================================================
    """

    """
    STATE=Integer(default=0)
    WAIT=Float(default=0.05)
    TIMEOUT=Float(default=0.05)

    START   =Function(TreeLogs())
    STOP    =Function(TreeLogs())
    JOIN    =Function(TreeLogs())

    PAUSE    =Function()
    IS_RUNNING    =Function()
    SEND    =Function()
    MESSAGE    =Function()
    MESSAGES    =Function()

    STATE_INIT=0
    STATE_READY=1
    STATE_DONE=2
    STATE_RUNNING=3
    STATE_PAUSE=4

    #----------------------------------------------------------------
    # life cycle
    #----------------------------------------------------------------

    def onSetup(self):

        #return
        self.lock=threading.Lock()
        #WARNING: pas d'heritage, a cause du lock, queues non trouvees
        self.process=threading.Thread(target=self.onProcess)

        self.__stop=False
        self.__messages=Queue()
        self.__result=list()

        self.state=IoNode.STATE_READY

    #----------------------------------------------------------------
    
    def onCleanup(self,**args):

        if self.is_running():
            self.stop()
        self.join(root=True)



    #----------------------------------------------------------------
    def onCall(self):
        """
        si le thread n'a pas démarré, demarrer
        """
        print("process")
        if not self.process.is_alive():
            yield self.start(root=True)


        try:
            yield self.join(root=True)
        except KeyboardInterrupt:
            yield self.stop()
            yield self.join(root=True)
        except:
            yield self.stop()
            yield self.join(root=True)

        node=Log_Message(name=self.name+"_run")
        for msg in self.children.by_class("Log_Message"):
            msg.parent=node
        yield node
        for elt in self.all().by_class("IoNode"):
            node=Log_Message(name=elt.name+"_run")
            for msg in elt.children.by_class("Log_Message"):
                msg.parent=node
            yield node

        #self.tree()

    #----------------------------------------------------------------
    # controls
    #----------------------------------------------------------------

    def start(self,root=True):
        if self.state <IoNode.STATE_READY:
            self.setup().parent=self
        
        for elt in self.get_firsts(cls=IoNode,root=False):
            yield elt.query("start")

        if root == True and self.state<IoNode.STATE_RUNNING:

            self.process.start()
            self.state=IoNode.STATE_RUNNING


    #----------------------------------------------------------------
    def join(self,root=False):

        yield self.children.by_class(IoNode).query("join",root=True)

        if root == True:
            if self.process and self.process.is_alive()==True:
                self.process.join()

            for elt in self.__result:
                elt   
            del  self.__result
            self.__result=list()

        self.process=None

        #print(self.path(),"join")
    #----------------------------------------------------------------
    def pause(self):
        self.lock.acquire()
        if self.state==IoNode.STATE_PAUSE:
            self.state=IoNode.STATE_RUNNING
        elif self.state==IoNode.STATE_RUNNING:
            self.state=IoNode.STATE_PAUSE
        self.lock.release()
    #----------------------------------------------------------------

    def stop(self):
        "order to stop the process. Stops the while loop inside run() function"

        self.lock.acquire()
        #self.__stop=True    
        self.state=IoNode.STATE_DONE  
        self.lock.release()

        yield self.children.by_class(IoNode).query("stop")


    #----------------------------------------------------------------
    def is_running(self):

        if hasattr(self,"process") and self.process and  self.process.is_alive():
            self.lock.acquire()
            result=(self.state>IoNode.STATE_DONE)   
            self.lock.release()
            return result
        return False

    #----------------------------------------------------------------
    def get_state(self):

        if hasattr(self,"process") and self.process and  self.process.is_alive():
            self.lock.acquire()
            result=self.state  
            self.lock.release()
            return result
        else:
            return self.state  


    #----------------------------------------------------------------
    # BEHAVIOR
    #----------------------------------------------------------------
    def onProcess(self,**args):

        time.sleep(self.wait)
        self.onStart()

        while self.state>IoNode.STATE_DONE:


            if self.state<IoNode.STATE_PAUSE and self.test()==True:

                self.onDoChildren()
                self.onDo()

            time.sleep(self.timeout)
        self.onStop()

    #----------------------------------------------------------------
    def checkAction(self):
        return self.state>IoNode.STATE_DONE and self.state<IoNode.STATE_PAUSE

    #----------------------------------------------------------------

    def onStart(self):
        pass        

    #----------------------------------------------------------------

    def onDoChildren(self):

        for child in self.children.by_class(Code):
            if self.checkAction()==True:
                if not isinstance(child,IoNode):
                    r=child.call()
                    r.parent=self

    #----------------------------------------------------------------

    def onDo(self):
        pass
    #----------------------------------------------------------------

    def onStop(self):
        pass


    #----------------------------------------------------------------
    #pipes
    #----------------------------------------------------------------
    def new_message(self,message=None,cls=Log_Message,**args):

        if message is None:
            message=cls(**args)

        if type(message) is str:
            message=cls(message=message,**args)

        #if hasattr(message,"setup"):message.setup()
        message.update(args)
        message.data_source=self.path()
        message.data_tag=self.path().replace("/","_")
        if message.data_tag!="":message.data_tag=self.name
        return message

    #----------------------------------------------------------------
    def send_to_pipe(self,tag=None,message=None,**args):
        "envoyer un message à tous les pipes sortants"

        if message is None:
            message=self.new_message(**args)

        for pipe in self.outputs.by_class(Pipe):
            if tag is None:
                pipe.send_message(message)
            elif pipe.tag==tag:
                pipe.send_message(message)

    #----------------------------------------------------------------
 
    def logs(self,parent=True,cls=Log_Message,**args):

        message=self.new_message(cls=cls,**args)

        if parent==True:
            message.parent=self
        else:
            self.send_to("/",message=message)
    #----------------------------------------------------------------

    def send(self,message=None,**args):
        "send a message to node"
        if message is None:
            message=self.new_message(**args)

        self.__messages.put(message)


    #----------------------------------------------------------------
 
    def send_to(self,path,message=None,**args):

        message=self.new_message(**args)
        self.find(path).send(message=message)

    #----------------------------------------------------------------

    def message(self):
        "get one message from message box"

        try:
            message=self.__messages.get(timeout=self.wait)
            self.__messages.task_done()
            return message

        except:
            return None

    #----------------------------------------------------------------

    def messages(self):

        r=True
        while r is not None:
            #time.sleep(DT)
            r=self.message()
            if r is not None:
                yield r 
  
    #----------------------------------------------------------------
#===================================================================


