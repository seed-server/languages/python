from dataModel.models import *
from core.program import *
from core.data import *

#==========================================================

class FlaskRequest(Data):
    
#==========================================================
    METHOD=String()
    QUERY_ARGS=String()
    REQUEST_CONTEXT=Field()
    #-------------------------------------------------------
    def onCleanArgs(self,**args):

        for k,v in args.items():
            if type(v)==list:
                if len(v)==1:
                    yield k,v[0]
                else:
                    yield k,v
            else:
                yield k,v

    #-------------------------------------------------------
    def setup(self):

        #self.url_args=args
        self.query_args=dict(self.onCleanArgs(**self.request_context.args))
        self.url_method=self.request_context.method
        self.response=""
        self.tree_node=None
        self.module=None
        self.method=None
        self.done=False
    #-------------------------------------------------------
    def call(self):
        return self.response
    #-------------------------------------------------------

#==========================================================
