

import jinja2
from jinja2 import Environment, FileSystemLoader
from flask import Flask,render_template,request
from flask import Response
from flask import jsonify,send_from_directory
from flask_cors import CORS

import urllib.request,os,time
import time

from dataModel.models import *
from core.program import *
from .FlaskInterface import FlaskInterface
from .FlaskModule import FlaskModule
from .FlaskRequest import FlaskRequest
#============================================================

class FlaskServer(Run,FlaskInterface):

#============================================================
    """
    root node for flask server, a tree of Server_Node to
    build an application
    """

    IP=String(default="localhost")
    PORT=Integer(default=8000)
    DEBUG=Boolean(default=True)
    TEMPLATE_FOLDER=String(default=None)

    #-------------------------------------------------------
    def get_server(self):
        return self
    #-------------------------------------------------------
    def onPreSetup(self):
    #-------------------------------------------------------
        #print("#"*50,self.path())

        #self.setEnviron()
        #self.setCommandLine()
        #self.processTree()


        self.app = Flask(self.name.replace(".","_"),template_folder=self.template_folder)
        CORS(self.app)
        self.app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1


        self.app.add_url_rule("/","main", self.ask,['GET','POST'])   
        self.app.add_url_rule("/<path:path>","main_path", self.ask_path,['GET','POST','PUT','DELETE'])   



    #-------------------------------------------------------
    def onPostSetup(self):
    #-------------------------------------------------------
        #print("setup done")
        #self.tree()
        pass
    #-------------------------------------------------------
    def onCall(self):
    #-------------------------------------------------------

        self.app.run(host=self.ip,
                        port=int(self.port),
                        debug=self.debug, 
                        threaded=True, 
                        use_reloader=False)


    #-------------------------------------------------------
    def ask(self,**args):
    #-------------------------------------------------------
        return self.onProcessRequest(select="/",request_context=request,**args)

    #-------------------------------------------------------
    def ask_path(self,path,**args):
    #-------------------------------------------------------
        return self.onProcessRequest(select="/"+path,request_context=request,**args)


    #-------------------------------------------------------
    def onProcessRequest(self,**args):
    #-------------------------------------------------------
        request=FlaskRequest(parent=self,server=self,**args)
        request.setup()

        for module in self.children.by_class(cls=FlaskModule):
            if module.onTestRequest(request)==True:
                module.onProcessRequest(request)

        return request.call()



#============================================================
