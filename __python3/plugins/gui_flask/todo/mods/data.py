from ..modules import Api_Module
from ..api import *
from dataModel import *
from dataModelAddons.tree import ChildrenFrame,Tree
#============================================================

class Api_Mod(Api_Module):

#============================================================
    SOURCE=String(default="/")

    #-------------------------------------------------------
    def onSetup(self,**args):
        self.object=self.find(self.source,cls=Atom)

    #-------------------------------------------------------
    def search_node(self,path,**args):

        if path is not None and path.strip() !="/":
            return self.object.find(path)
        else:
            return self.object
    #-------------------------------------------------------
    def node_infos(self,node):
        return dict(name=node.name,path="/"+node.path())
    #-------------------------------------------------------

#============================================================

class Api_GetAttr(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")
        Api_Argument(parent=self,name="attr",cls="str")

    def onResponse(self,request,module=None,node="/",attr=None,**args):

        node=self.find(node)

        if attr is None:
            r=dict(node.getRecordableDict())
            r["__class__"]=node.__class__.__name__
            return  r

        attribute=node[attr]

        if isinstance(attribute,Store):
            r=list()
            for elt in attribute:
                r.append(module.node_infos(elt))
            print(r)
            return dict(list=r)

        elif isinstance(attribute,Atom):
            return module.node_infos(attribute)

        else:
            return  attribute

    #-------------------------------------------------------


#============================================================

class Api_Set(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")
        Api_Argument(parent=self,name="attr",cls="string")
        Api_Argument(parent=self,name="value",cls="*")

    def onResponse(self,request,module=None,node="/",attr=None,value=None,**args):
        node=self.find(node)
        node[attr]=value
        return dict()
#============================================================

class Api_CreateLink(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")
        Api_Argument(parent=self,name="source",cls="path")
        Api_Argument(parent=self,name="target",cls="path")
        Api_Argument(parent=self,name="cls",cls="cls")

    def onResponse(self,request,module=None,node="/",source=None,target=None,cls="Atom_Link",**args):


        node=self.find(node)

        if source :
            source=self.find(source)
        if target:
            target=self.find(target)

        cls_obj=getClass(cls)

        if source and target:
            obj=cls_obj(parent=node,source=source,target=target,**args)        
        return dict()


#============================================================

class Api_New(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")
        Api_Argument(parent=self,name="cls",cls="cls")


    def onResponse(self,request,module=None,node="/",cls="Atom",name=None,**args):

        node=self.find(node)

        if name and node.find(name):
            return dict()
        else:
            cls_obj=getClass(cls)
            obj=cls_obj(parent=node,name=name,**args)        
            return  dict(msg="creation",path=obj.path())

#============================================================

class Api_Action(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")
        Api_Argument(parent=self,name="action",cls="string")
    #-------------------------------------------------------

    def onResponse(self,request,module=None,node="/",action=None,**args):
        node=self.find(node)

        from content import Content

        data=dict()
        if not action:
            data.update(path="/"+node.path(),**dict(node.getRecordableDict()) )

        else:
            result= node.query(action=action,**args)


            if isinstance(result,Frame):
                return self.frame_to_json(result)
            elif isinstance(result,Content):
                data["data"]=result.get_content()
            elif isinstance(result,Tree):
                result.parent=self.append("logs",parent=self)

                data["data"]=result.path()

            elif type(result) is dict:
                data.update(result)
            else:
                data["data"]=result
        #print(data)
        return data
#============================================================

class Api_Help(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node",cls="path")

    #-------------------------------------------------------

    def onResponse(self,request,module=None,node="/",action=None,**args):

        node=self.find(node)

        return node.help(Atom)
#============================================================

