from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .Axe import Axe


#==========================================================

class Graphic(Object):
    
#==========================================================
    """

    """

    #------------------------------------------------------------
    def make(self):
        yield "CURVE "+self.path()
        yield self.onMake()

    #------------------------------------------------------------
    def onMake(self):
        pass

    #------------------------------------------------------------
    def clear(self):
        return self.onClear()
    #------------------------------------------------------------
    def onClear(self):
        return

    #------------------------------------------------------------
    def infos(self):
        return self.onInfos()

    #------------------------------------------------------------
    def onInfos(self):
        return

    #------------------------------------------------------------
    def get_axes(self):
        return self.children.by_class(Axe)

   #------------------------------------------------------------



#==========================================================
