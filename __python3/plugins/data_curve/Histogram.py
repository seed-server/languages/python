from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .Graphic import Graphic

#=================================================================

class Histogram(Graphic):

#=================================================================

    LIMIT=Integer()#limite basse
    #------------------------------------------------------------
    def onMake(self):
        self.result=dict()
        yield "CURVE "+self.path()
        for node in self.children.by_class(Select):
            result=len(node.get_selection())
            if self.limit and result> self.limit:
                self.result[node.name]=result
            elif not self.limit:
                self.result[node.name]=result
    #------------------------------------------------------------
    def onInfos(self):
        return list(self.result.keys()),list(self.result.values())
    #------------------------------------------------------------

#=================================================================

class Distribution(Histogram):

#=================================================================

    ATTR=String()#limite basse
    #------------------------------------------------------------
    def onMake(self):
        self.result=dict()
        yield "CURVE "+self.path()
        for node in self.children.by_class(Select):
            for elt in node.get_selection():
                if self.attr in elt.keys():
                    v=elt[self.attr]
                    if not (v) in self.result.keys():
                        self.result[str(v)]=0
                    self.result[str(v)]+=1
                else:
                    if not (v) in result.keys():
                        self.result[str(None)]=0
                    self.result[str(None)]+=1

        if self.limit:
            for k,v in dict(self.result).items():
                if self.result[k]<self.limit:
                    del self.result[k]

    #------------------------------------------------------------

#=================================================================

