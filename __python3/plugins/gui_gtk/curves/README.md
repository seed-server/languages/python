
# Curves with matplotlib

- [exemples](https://matplotlib.org/stable/gallery/index)
- [couleurs](https://matplotlib.org/stable/gallery/color/named_colors.html)
- [tutos w3C](https://www.w3schools.com/python/matplotlib_intro.asp)
- [customize](https://matplotlib.org/stable/tutorials/introductory/customizing.html)
- [background](https://stackabuse.com/how-to-change-plot-background-in-matplotlib/)
- [styles](https://tonysyu.github.io/raw_content/matplotlib-style-gallery/gallery.html)
- [styles examples](http://www.futurile.net/2016/02/27/matplotlib-beautiful-plots-with-style/)
- [legende](https://www.statology.org/matplotlib-legend-position/)
- [them1](https://towardsdatascience.com/a-new-plot-theme-for-matplotlib-gadfly-2cffc745ff84)

