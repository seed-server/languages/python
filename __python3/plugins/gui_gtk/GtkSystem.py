from dataModel.models import *
from core.program import *
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gio
from plugins.gui.ViewSystem import ViewSystem

#==========================================================

class GtkSystem(ViewSystem):
    
#==========================================================
    """

    """

   #---------------------------------------------------
    def onBuild(self,node):
        """
        """
        yield "GTK start"

        yield node.build()
        yield node.redraw()

        Gtk.main()
#==========================================================

class GtkSystemQuit(Run):
    
#==========================================================
   #---------------------------------------------------
    def onCall(self):
        """
        """
        print( "INTERUPT "+self.path() )
        Gtk.main_quit()
        return "GTK quit"

   #---------------------------------------------------


#==========================================================
