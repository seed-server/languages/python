from dataModel.models import *
from ..GtkObject import GtkObject
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#==========================================================

class GtkPaned(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.HPaned
    V=Boolean(default=False)
    WIDTH=Integer(default=500)
    #-------------------------------------------------

    def onBuildView(self):

        if self.v==True:
            return Gtk.VPaned()
        else:
            return Gtk.HPaned()
    #-------------------------------------------------

    def onBuild(self,view_object,node):
        view_object.set_position(self.width)
    #-------------------------------------------------


#==========================================================
