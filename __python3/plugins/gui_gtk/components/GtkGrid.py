from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkGrid(GtkObject):
    
#==========================================================
    """

    """
    SX=Integer(default=1)
    SY=Integer()
    EXPAND_ALL=Boolean(default=False)
    #-------------------------------------------------
    def onBuildView(self):
        
        #self.position="add"
        self.x=1
        self.y=0
        node = Gtk.Grid()
        node.set_column_homogeneous(True)
        node.set_row_homogeneous(True)
        return node

    #-------------------------------------------------        
    def add(self,node):
        if self.expand_all == True:
            node.view_object.set_vexpand(True)
            node.view_object.set_hexpand(True)
        self.view_object.attach(node.view_object, self.x,self.y, 1, 1)
        self.x+=1
        if self.x > self.sx:
            self.x=1
            self.y+=1
#==========================================================
