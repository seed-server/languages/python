from dataModel.models import *
from core.program import *
from core.data import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk
import console

#==========================================================

class GtkNoteBookPage(GtkObject):
    
#==========================================================

    VIEW_CLASS=Gtk.Box
    ATTACH_PARENT=False
    TEXT=String()

    #-------------------------------------------------
    def onBuild(self,view_object,node):
        view_object.set_vexpand(True)
        view_object.set_hexpand(True)
        self.event_message(name=self.path(),result=self.call_view(self.text,node,self))
        self.parent.view_object.append_page(view_object, Gtk.Label(label=node.name))
    #-------------------------------------------------        

#==========================================================

class GtkNoteBook(GtkObject):
    
#==========================================================
    """

    """
    TEXT=String()
    VIEW_CLASS=Gtk.Notebook
    def onBuildX(self,view_object,node):
        view_object.set_tab_pos(Gtk.PositionType.LEFT)
    #-------------------------------------------------
    def onBuildX(self,view_object,node):
        view_object.set_vexpand(True)
        view_object.set_hexpand(True)

        page=GtkNoteBook_Page(parent=self,data_object=node,text=self.text)
        #page.setup()
        #page.tree()
        #page.build()
        #page.view_object.set_border_width(50)


    #-------------------------------------------------        
    def onBuildChildrenX(self):
        pass
    #-------------------------------------------------        

#==========================================================

