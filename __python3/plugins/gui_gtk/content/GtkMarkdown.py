from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from ..GtkObject import GtkObject
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk,Gio
from gi.repository.WebKit2 import WebView, Settings
import markdown
from core import text_parser
#==========================================================

class GtkMarkdown(GtkObject):
    
#==========================================================
    """

    """

   #---------------------------------------------------
    def onBuildView(self):
        """
        """
        self.fill=True
        self.expand=True
        #self.position="start"
        view_object = Gtk.VBox() 
        #view_object.set_policy(Gtk.POLICY_AUTOMATIC,Gtk.POLICY_AUTOMATIC) 
        return view_object

   #---------------------------------------------------
    def onBuild(self,view_object,node):
        wv = WebView() 
        wv.fill=True
        wv.expand=True
        #disable the plugins for the webview 
        ws = wv.get_settings() 
        ws.set_property('enable-plugins',False) 
        wv.set_settings(ws) 
        f=open(node.text,'r')
        content=f.read()
        f.close()

        content=text_parser.parse(node,content)
        mdtext = markdown.markdown(content) 
        wv.load_html(mdtext,"file:///") 

        view_object.add(wv) 

   #---------------------------------------------------
#==========================================================
