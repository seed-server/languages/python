from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk,Gio
from gi.repository.WebKit2 import WebView, Settings
#==========================================================

class GtkBrowser(GtkObject):
    
#==========================================================
    """

    """
    URL=String()


   #---------------------------------------------------
    def onBuildView(self):
        """
        """
        self.fill=True
        self.expand=True
        self.position="start"
        view_object = Gtk.ScrolledWindow() 
        return view_object

   #---------------------------------------------------
    def onBuild(self,view_object,node):
        wv = WebView() 
        #disable the plugins for the webview 
        ws = wv.get_settings() 
        ws.set_property('enable-plugins',False) 
        wv.set_settings(ws) 
        print(self.url%node)
        wv.load_uri(self.url%node)
        view_object.add(wv) 


   #---------------------------------------------------
    def onChangeUrl(self,wv,url):
        """
        """

        wv.load_uri(url)

   #---------------------------------------------------



#==========================================================
