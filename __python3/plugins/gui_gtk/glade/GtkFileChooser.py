from dataModel.models import *
from .GladeFile import GladeFile,GLADE_MODELS
from dataModel.Store import Store

#==========================================================

class GtkFileChooser(GladeFile):
    
#==========================================================
    """

    """
    URL=String()
    SYSTEM_FILE=GLADE_MODELS+"/files.glade"


   #---------------------------------------------------
    def onBuild(self,view_object,node):
        """
        """
        self.onChangeUrl(self.url%node)

   #---------------------------------------------------
    def onChangeUrl(self,string):
        """
        """
        print(string)
        view_object=self.get_builder().get_object("content")


        if "://" in string:
            view_object.set_current_folder_uri(string)
        else:
            view_object.set_current_folder(string)

   #---------------------------------------------------



#==========================================================
