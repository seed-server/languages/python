from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store

#==========================================================

class GladeObject(GtkObject):
    
#==========================================================
    """

    """
    ATTACH_PARENT=False

    #-----------------------------------------------------     
    def onBuildView(self):
        self.attach_parent=False
        view_object=self.get_object(self.name)
        if view_object is None:
            raise Exception("no glade object",self.name)

        return view_object

    #-----------------------------------------------------     
    def get_object(self,node_id):
        return self.get_builder().get_object(node_id)
    #-----------------------------------------------------     
    def get_builder(self):
        return self.parent.get_builder()

    #-------------------------------------------------        

#==========================================================
