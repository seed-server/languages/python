from .settings import ALL_CLASSES
from .Field import Field
from .Function import Function

def get_bases(cls):

    lst=[]
    for elt in cls.__bases__:
        if elt != object:
            lst.append(elt.__name__)
            lst.extend(get_bases(elt))
    return lst

#=============================================================================

class ModelMeta(type):

#=============================================================================
    """
    ModelMeta est une meta classe, elle permet de construire les modèles.

    * une interface de programmation

    une classe ayant comme meta model ModelMeta, pourra être manipulée par
    un emsemble de commandes standards quelquesoit sa structure ainsi 
    que celle de ses instances.

    Des structures prédéfinies telles que Arbre et Réseau pourront être combinées.
    voir les Stuctures : "structures"
    voir la combinaison arbre réseau : "atom.model"

    elle collecte les attributs de classe héritant de Fields et Function 
    pour creer une interface de programmation dynamique pour chaque classe.

    * héritage de ModelMeta

    les différents modèles doivent déclarer ModelMeta comme metaclasse ::

       class MyModel(metaclass=ModelMeta):

            #déclaration des champs
            A=Field(int)
            B=Field(str)

            def method(self):
                "utilisation des champs"
                self.a=2
                print(self.a)

    les modèles sont des classes abstraites qui doivent etre combinées pour
    devenir utilisable.
    les modèle obligatoire est Model auquel on ajoute les autres modèles.

    le modèle MyModel doit etre utilisé avec la classe Model pour fonctionner ::

       class ConcreteClass(Model,MyModel):
           pass

    note sur l'écriture des variable :
       - variables en majuscule : les variables de classe avec Field
       - minuscule : les variables d'instance Attribute


    cette classe a été introduite pour gerer l'héritage multiple
    en python
    """

    #--------------------------------------------------------------------------

    CLASSES=list()

    #--------------------------------------------------------------------------
    def __init__(cls, name, bases, ns):
        """
        store the field definitions on the class as a dictionary

        mapping the field name to the Field instance.
        """

        if cls not in ALL_CLASSES.values():
            ALL_CLASSES[cls.__name__]=cls

        cls._fields = {}
        cls._functions = {}
        cls.callbacks = {}

        # loop through the namespace looking for Field instances
        for key, value in ns.items():

            if isinstance(value, Field):
                cls._fields[key.lower()] = value
                value.name=key.lower()
                #value.doc=getattr(cls,key.lower()).__doc__

            elif isinstance(value, Function):
                cls._functions[key.lower()] = value
                value.name=key.lower()
                value.doc=getattr(cls,key.lower()).__doc__

            elif key.startswith("on"):
                cls.callbacks[key] = value

            else:
                pass#print(key,value)


    #--------------------------------------------------------------------------
    def __help__(cls):
        bases=get_bases(cls)
        data={}
        fields={}
        functions={}
        callbacks={}


        for k,v in cls._fields.items():
            fields[ k ] = v.__help__()

        for k,v in cls._functions.items():
            functions[k]=v.__help__()

        for k,v in cls.callbacks.items():
            callbacks[ k ] = v.__doc__


        data["doc"]=cls.__doc__



        if len(bases)>0:
            data["bases"]=bases

        if len(fields)>0:
            data["fields"]=fields
        if len(functions)>0:
            data["functions"]=functions

        if len(cls.callbacks)>0:
            data["callbacks"]=callbacks

        return data
    #--------------------------------------------------------------------------
    
#=============================================================================
