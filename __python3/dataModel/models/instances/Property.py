#=============================================================================

class Property(object):

#=============================================================================

    """
    L'attibut est créé par le champs Field correspondant.
    il n'est jamais créé directement.

    son nom est en minuscule

    gère les callback


    """
    #--------------------------------------------------------------------------
    def __init__(self, node,field):
        """
        creer un attribut pour le dictionnaire node avec le champs field.

        Attribut gère les appels à la valeur réelle :

           - contient la valeur de l'attribut
           - les fonctions réelles du noeud node sont assignées au fonctions callback
        """

        #instance attributes

        self.node = node
        self.field = field
        self.value = None


        #get real callback functions

        #modify
        if self.field.set_f is not None:
            self.set_f=getattr(self.node,self.field.set_f)
        else:
            self.set_f=None

        #access
        if self.field.get_f is not None:
            self.get_f=getattr(self.node,self.field.get_f)
        else:
            self.get_f=None

        #delete
        if self.field.del_f is not None:
            self.del_f=getattr(self.node,self.field.del_f)
        else:
            self.del_f=None
    #--------------------------------------------------------------------------

    def init(self,value=None):
        """
        valider les valeurs assignées aux champs
        """
        self.modify(value)
        #print("set value",self.node.__class__.__name__,self.field.name,self.value)

    #--------------------------------------------------------------------------

    def is_valid(self,value):

        #acceptable value None
        if value is None:
            return True

        #if a class declared
        elif  self.field.cls is not None:

            #if a list of classes
            if type(self.field.cls)==list:
                for c in cls:
                    if isinstance(value, c)==True:
                        return True

            #if single class
            else:
                return isinstance(value, self.field.cls)

        #no constrain, ok
        else:
            return True

        return False

    #--------------------------------------------------------------------------

    def validate(self):
        """
        valider les valeurs assignées aux champs
        """
        return self.is_valid(self.access())
    #--------------------------------------------------------------------------

    def access(self):

        "fonction executée quand l'attibut est appelé, retourne la valeur"

        if self.get_f is not None:
            return self.get_f(self.value)
        else:
            return self.value

    #--------------------------------------------------------------------------

    def modify(self,value):

        "fonction executée quand l'attibut est modifié, prend en argument la valeur"

        if self.set_f is not None:
            self.value=self.set_f(value)
            return


        self.value=self.field.get_value(value)

        return

    #--------------------------------------------------------------------------

    def delete(self):

        "fonction executée quand l'attibut est supprimé"

        if self.del_f is not None:
            self.value=self.del_f(self.value)
        else:
            pass

    #--------------------------------------------------------------------------

    def __str__(self):
        return str(self.value)
    #--------------------------------------------------------------------------

#=============================================================================

