# -*- coding: utf-8 -*-
"""
    dans l'esprit ORM sqlalchemy et django 

    ajouter des modèles aux noeuds de l'arbre :
       - modifier dynamiquement les attributs et leur valeur, grouper les attributs
       - creer un systeme d'évenement/action évolutif sur les objets


"""

from .settings import getClass
from .ModelMeta import ModelMeta
from .Function import Function
from .behaviors.FieldManager import FieldManager
from .behaviors.ModelManager import ModelManager
from .behaviors.FunctionManager import FunctionManager
from .instances.Property import Property
from .fields.basics import String

#=============================================================================

class Model(ModelManager,FunctionManager,FieldManager,
                metaclass=ModelMeta):

#=============================================================================

    """
    la classe Model est la classe concrète dont chaque structure doit
    hériter pour utiliser le système.

     elle permet de construire des types de données ::

       class SomeModel(Model):
            A=Field(int)
            B=Field(str)

    elle gère la création des instances.

    """
    CLASS_NAME=String(doc="nom de la classe dont hérite l'instance")


    #TODO:couille avec function et args
    #UPDATE=Function()
    DESTROY=Function()

    #--------------------------------------------------------------------------

    def __init__(self,**args):

        """
        pour toutes les bases de la classe réelle, rechercher des champs
        et création du dictionnaire d'attibut.
        """

        FieldManager.onInitFields(self)

        #liste des champs pour le modele
        fields=dict( self.__class__.getFields() )
        functions=dict( self.__class__.getFunctions() )

        FieldManager.__init__(self)

        #creer les attibuts
        for k,v in fields.items():

            v.new_attribute(self)

        #creer les fonctions
        for k,v in functions.items():
            
            v.setFunction(self)


        self.class_name=self.__class__.__name__

        #assigner les valeurs
        for k,v in args.items():
            self[k]=v

        for mod in self.models():
            if mod != Model and hasattr(mod,"onInit"):
                getattr(mod,"onInit")(self)



    #--------------------------------------------------------------------------
    def onInit(self):
        """
        fonction d'initialisation des classes MetaModel
        la fonction est appelée à la création de l'instance du modèle
        ne fait rien
        """
        pass


    #--------------------------------------------------------------------------

    def destroy(self):
        """
        destruction du modèle
        appelle les fonctions onDestroy des MetaModel pour détruire
        chaque partie du modèle
        """

        for mod in self.models():
            if mod != Model and hasattr(mod,"onDestroy"):
                getattr(mod,"onDestroy")(self)

        FieldManager.onDestroyFields(self)

    #--------------------------------------------------------------------------
    def getAbstracts(self):
        for k,v in ALL_CLASSES.items():
            if type(v)==ModelMeta and not issubclass(v,Model):
                yield v
    #--------------------------------------------------------------------------
    def is_instance(self,cls,strict=False):

        cls=getClass(cls)
        if strict==True:
            return self.__class__==cls


        return isinstance(self,cls)




    #--------------------------------------------------------------------------
    def getClasses(self,cls=None):

        if cls is not None:
            cls=getClass(cls)
            d=dict()
            for k,v in ALL_CLASSES.items():
                if issubclass(v,cls):
                    d[k]=v
            return d
        else:
            return ALL_CLASSES

    #--------------------------------------------------------------------------

    def validate(self):
        """
        valider les valeurs assignées aux champs
        """

        for k,v in self.attributes():
            if v.validate() == False:
                return False
        return True

    #--------------------------------------------------------------------------

    def check_errors(self):
        """
        valider les valeurs assignées aux champs
        """

        for k,v in self.attributes():
            if v.validate() == False:
                yield k,v

    #--------------------------------------------------------------------------

    def attributes(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(FieldManager.items(self)):

            if isinstance(v,Property):
                yield k,v

    #--------------------------------------------------------------------------

    def getNonAttributeDict(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(FieldManager.items(self)):
            if not isinstance(v,Property):
                yield k,v
    #--------------------------------------------------------------------------

    def getRecordableDict(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(FieldManager.items(self)):
            if isinstance(v,Property) and v.field.record == True:
                yield k,v.access()
            if not isinstance(v,Property) and not isinstance(v,Frame):
                yield k,v
    #--------------------------------------------------------------------------

    def __setattr__(self, key, value):

        """
        on assigne une valeur à l'attribut ::

           self.attr=2
        """


        if "_FieldManager__data" in self.__dict__.keys():
            data=self.__dict__["_FieldManager__data"]

            if key in data.keys():
                if isinstance(data[key],Property):
                    data[key].modify(value)
                    return
                else:
                    data[key]=value
                    return

        object.__setattr__(self,key, value)



    #--------------------------------------------------------------------------

    def __getattr__(self, key):

        """
        on recupère la valeur de l'attribut ::

           value=self.attr
        """

        if "_FieldManager__data" in self.__dict__.keys():
            data=self.__dict__["_FieldManager__data"]

            if key in data.keys():
                value = self.__dict__["_FieldManager__data"][key]
                if isinstance(value,Property):
                    return value.access()
                else:
                    return value

        return self.__getattribute__(key)




    #--------------------------------------------------------------------------

    def __delattr__(self, key):

        """
        on assigne une valeur à l'attribut ::

           del self.attr
        """

        try:
            FieldManager.__delattr__(self,key)
        except:

            del self[key]

    #--------------------------------------------------------------------------
    def values(self):

        """ 
        liste les valeurs des attributs ::

           for elt in  node.values():
               ...
        """

        for elt in self.__data.values():
            if isinstance(elt,Property):
                yield elt.access()
            else:
                yield elt

    #--------------------------------------------------------------------------
    def items(self):

        """ 
        liste des tuples (chemins,noeud) ::

           for key,elt in  node.items():
               ...   
        """

        for k,elt in FieldManager.items(self):
            if isinstance(elt,Property):
                yield k,elt.access()
            else:
                yield k,elt

    #--------------------------------------------------------------------------

    def __getitem__(self,key):

        """
        lire une donnée, déclenche les évènements attachés::

            a = node["a"]


       Les champs déclarés avec Field sont enregistrés 
        avec Property dans le dictionnaire. la lecture fait appel à la fonction access
    On ne récupère jamais l'objet Property.
        Les autres sont lus tel quel.

    L'accès aux données
         """


        node=FieldManager.__getitem__(self,key)

        if isinstance(node,Property):
            return node.access()
        else:
            return  node

    #--------------------------------------------------------------------------

    def __setitem__(self,key,value):

        """
        modifier une donnée, déclenche les évènements attachés ::

           node["a"] = 2

        Les champs déclarés avec Field sont enregistrés 
        avec Property dans le dictionnaire. fait appel à la fonction modify

        Les autres sont enregistrés tel quel.
        """

        if key in self.keys():
            node=FieldManager.__getitem__(self,key)

            if isinstance(node,Property):
                return node.modify(value)

        FieldManager.__setitem__(self,key,value)

    #-------------------------------------------------------------------------

    def __delitem__(self,key):

        """
        supprimer une donnée , déclenche les évènements attachés::

            del node["a"]
        """


        if key in self.keys():
            node=FieldManager.__getitem__(self,key)

            if isinstance(node,Property):
                return node.delete()

        Node.__delitem__(self,key)

#-------------------------------------------------------------------------

#=============================================================================

