#=============================================================================

class Decorator(object):

#=============================================================================
    """
    base class to make decorators
    """
    def __init__(self,**kwargs):
        self.kwargs=kwargs
        self.function=None


    def init(self,node,function):
        "function to verload to have behavior"
        self.function=function
        self.node=node

    def __call__(self,*args,**kwargs):
        #print("deco",self,args,kwargs)
        return self.onCall(self.function,args,kwargs,**self.kwargs)

    def onCall(self,function,args,kwargs,**options):
        return function(*args,**kwargs)

    def __help__(self):
        return dict(name=self.__class__.__name__,doc=self.__class__.__doc__,args=self.kwargs)

#=============================================================================
