from dataModel.models import *

#=============================================================================

class ParentProperty(Property):

#=============================================================================


    #--------------------------------------------------------------------------

    def modify(self,value):

        "reparent current node"

        self.delete()

        # et node existe
        if  value is not None:
            #nouvel attachement
            value.children.append(self.node)
            self.value=value



    #--------------------------------------------------------------------------

    def delete(self):

        "fonction executée quand l'attibut est supprimé"

        # si attache, detacher
        if self.value is not None:
            self.value.children.remove(self.node)
            self.value=None

    #--------------------------------------------------------------------------

#=============================================================================

class Parent(Field):

#=============================================================================

    Attribute=ParentProperty

#=============================================================================
