from dataModel.models import *
from dataModel.Store import Store
from .NamedTree import NamedTree
import os
import xml.etree.ElementTree as ET

#==========================================================

class LoadError(Model,NamedTree):
    
#==========================================================
    pass
#=======================================================================

class TreeLoader(metaclass=ModelMeta):

#=======================================================================

    """


    """


    #-----------------------------------------------------
    def open(self,filename,root,first=False,recursive=False,extention=None,cls=None):

        if os.path.isfile(filename) and extention and filename.endswith(extention):

            yield "    LOAD "+filename
            yield self.open_file(filename,root)

        elif first == True or (recursive==True and os.path.isdir(filename)):

            for elt in os.listdir(filename):
                p=os.path.join(filename,elt)
                if first !=True and ( recursive==True and os.path.isdir(p) ):
                    root=getClass(cls)(parent=root,name=elt)

                yield self.open(p,root,recursive=recursive,extention=extention,cls=cls)

    #-----------------------------------------------------
    def open_file(self,filename,root):

            try:
                elt=ET.parse(filename).getroot()
            except Exception as e:
                print(filename)
                raise e
            yield self.parse_xml(elt,parent=root)
    #-----------------------------------------------------
    def parse_xml(self,xmlnode,**kwargs):

        if hasattr( xmlnode , 'attrib'): 
            attrib=dict(xmlnode.attrib )
            #attrib=dict(convert_dict(xmlnode.attrib ))
        else:
            attrib=None

        #tag
        if hasattr( xmlnode , 'tag'): 
            tag=xmlnode.tag
            attrib["xml_class"]=tag
        else:
            tag=None

        #text
        if hasattr( xmlnode , 'text'):

            if xmlnode.text is not None:    
                text=xmlnode.text.strip()
                if text !="":
                    attrib["text"]=text

        #convert attributes
        attrib.update(kwargs)

        for k,v in attrib.items():
            try:
                attrib[k]=v#eval(v)
            except:
                pass

        try:
            cls=getClass(tag)
        except:
            cls=LoadError
            if not "name" in attrib.keys():
                attrib["name"]="Error_"+tag
            else:
                attrib["name"]="Error"+attrib["name"]
            yield "    ERROR CLASS "+tag+" not exists"
        node=cls(**attrib)

        for xmlchild in xmlnode:
            yield self.parse_xml(xmlchild,parent=node)


    #-----------------------------------------------------

#=======================================================================


