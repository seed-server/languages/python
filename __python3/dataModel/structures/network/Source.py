from dataModel.models import *
from .NetworkNode import NetworkNode
#=============================================================================

class SourceAttribute(Property):

#=============================================================================


    #--------------------------------------------------------------------------

    def modify(self,value):

        self.delete()

        if type(value) in [str]:
            value=self.node.onAttachSource(value)

        # et node existe
        if isinstance(value,NetworkNode):
            #nouvel attachement
            value.outputs.append(self.node)

        self.value=value

    #--------------------------------------------------------------------------

    def delete(self):


        # si attache, detacher
        if isinstance(self.value,NetworkNode):
            self.value.outputs.remove(self.node)
        self.value=None

    #--------------------------------------------------------------------------

#=============================================================================


class Source(Field):Attribute=SourceAttribute
