from dataModel.models import *
from .Local import Local
from .File import File
import os
RES_EXECPTIONS=["__pycache__",".pyc"]
#==============================================================
class Dir(Local):
#==============================================================

    RECURSIVE=Boolean(default=True)

    #-----------------------------------------------------
    def exists(self):
        return Local.exists(self) and os.path.isdir(self.text)
    #-----------------------------------------------------
    def onSetup(self):
        self.basename,self.extention=self.name,None
        yield Local.onSetup(self)
        self.__update()


    #-----------------------------------------------------
    def __update(self):
        if self.recursive ==True and self.exists()==True:

            nodes=list(self.children)

            lst=list(os.listdir(self.text))
            lst.sort()
            for elt in lst:
                if self.check_name(elt)==True:

                    p=os.path.join(self.text,elt)
                    node=self.find(elt)
                    if node:
                        nodes.remove(node)
                    elif os.path.isfile(p):
                        File(parent=self,name=elt,text=p)
                    elif os.path.isdir(p):
                        node=Dir(parent=self,name=elt,text=p,recursive=self.recursive)
            for elt in nodes:
                elt.destroy()
            del nodes
        else:
            self.clear()
            #return self.error()
    #-----------------------------------------------------

    def check_name(self,name):
        if name .startswith("."):
            return False

        for ex in RES_EXECPTIONS:
            if ex in name:
                return False
        return True
#==============================================================
