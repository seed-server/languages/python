from dataModel.models import *
from .Local import Local
import os
import mimetypes
#==============================================================
class File(Local):
#==============================================================



    #-----------------------------------------------------
    def onSetup(self):
        Local.onSetup(self)
        if "." in self.name and not self.name.startswith("."):
            i=self.name.rfind(".")
            self.basename,self.extention=self.name[:i],self.name[i+1:].lower()
        else:
            self.basename,self.extention=self.name,""
    #-----------------------------------------------------
    def exists(self):
        return Local.exists(self) and os.path.isfile(self.text)
    #-----------------------------------------------------

    def get_content(self):
        try:        
            f=open(self.text)
            content=f.read()
            f.close()
            return content

        except IOError:
            return self.error(text="can't find file "+self.text)

    #-----------------------------------------------------
    def get_type(self):
        return mimetypes.guess_type(self.text)[0]

    #-----------------------------------------------------
    def get_extention(self):
        return self.name.split(".")[1]

    #-----------------------------------------------------

#==============================================================
