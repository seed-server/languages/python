from dataModel.models import *
from .Data import Data

#==============================================================
class Resource(Data):
#==============================================================

    TEXT=String()

    #-----------------------------------------------------
    def exists(self):
        return self.text is not None
    #-----------------------------------------------------
    def get_type(self):
        pass
    #-----------------------------------------------------
    def get_content(self):
        return self.text
    #-----------------------------------------------------
    def get_extention(self):
        basename=self.text.split("/")
        if "." in basename:
            return basename.split(".")[1]
        return
    #-----------------------------------------------------

#==============================================================
