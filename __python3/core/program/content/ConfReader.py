from dataModel.models import *
from core.data import Data
from .Reader import Reader

#==============================================================    
class ConfNode(Data):
#==============================================================

    TEXT=String()

#==============================================================
class ConfReader(Reader):
#==============================================================
    """
    lecteur pour les textes bruts
    """

    #-----------------------------------------------------
    def onRead(self,node):
        """
        attache les lignes Line à la ressource
        """
        content=node.get_content()
        result=node

        for line in content.split("\n"):

            if line.startswith("["):

                i=line.find("[")
                j=line.find("]")
                name"=line[i+1:j].strip()
                result=ConfNode(parent=result,name=name)
            else:

                k,v=line.split(delimiter)
                ConfNode(parent=result,name=k,text=v)

        return "READ "+node.text
    #----------------------------------------------------------------



#==============================================================
