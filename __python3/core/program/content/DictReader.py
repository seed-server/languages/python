from dataModel.models import *
from core.data import Data
from .Reader import Reader
import json
#==============================================================    
class Dict(Data):
#==============================================================

    pass

#==============================================================    
class DictElement(Data):
#==============================================================

    TEXT=String()


#==============================================================
class DictReader(Reader):
#==============================================================
    """
    lecteur pour les textes bruts
    """

    #-----------------------------------------------------
    def onRead(self,node):
        """
        attache les lignes Line à la ressource
        """
        content=node.get_content()
        data=self.onReadDict(content)
        self.parse(node,data)
        return "READ "+node.text
    #-----------------------------------------------------
    def onReadDict(self,content):
        """
        
        """
        return dict(content=content)
    #----------------------------------------------------------------

    def parse(self,node,data):

        if type(data) is dict:
            for elt,txt in data.items():
                child=Dict(parent=node,name=i)
                self.parse(child,txt)
        if type(data) is list:
            i=0
            for elt in data.items():
                child=Dict(parent=node,name=i,txt=elt)
                i+=1
        else:
            child=DictElement(parent=node,name=i,text=elt)

    #----------------------------------------------------------------


#==============================================================
