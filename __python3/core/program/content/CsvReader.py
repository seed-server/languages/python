from dataModel.models import *
from core.data import Data
from .Reader import Reader



#==============================================================
class CsvReader(Reader):
#==============================================================
    """
    lecteur pour les textes bruts
    """
    DELIMITER=String(default=",")
    HEADERS=Field()



    #-----------------------------------------------------
    def onRead(self,node):
        """
        attache les lignes Line à la ressource
        """
        content=node.get_content()
        content=content.split("\n")
        if self.headers is None:
            self.headers=content[0].split(self.delimiter)
            self.headers=[elt.strip() for elt in self.headers ]
            content = content[1:]

        i=0
        for row in content:

            if row.strip()!="":
                row=row.split(self.delimiter)
                row=row[:len(self.headers)]
                row=[elt.strip() for elt in row ]
                data=dict(zip(self.headers,row))

                data=Data(parent=node,name=i,**data)

                i+=1
        return "READ "+node.text
    #-----------------------------------------------------


#==============================================================
