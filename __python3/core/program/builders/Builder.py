from dataModel.Store import Store
from dataModel.models import *
from core.data import *

from ..Object import Object
from ..handlers.Select import Select
from ..handlers.Handler import Handler
from ..tree.Run import Run

#==========================================================

class Builder(Select):
    
#==========================================================
    """
    manipuler des arbres en sélectionnant des éléments qui seront
    transmis aux Builder enfants pour effectuer des opérations
    """
    CLS=String(default="Data")
    ROOT_OBJ=Field(record=False)

   #---------------------------------------------------
    def onCall(self):

        if self.root_obj is None:
            self.root_obj=self.get_new_obj()
        yield Select.onCall(self)

        if self.parent and not isinstance(self.parent,Builder):
            yield self.onBuild(self.root_obj)

   #---------------------------------------------------
    def onCallChildren(self):
        """
        bloque la propagation de call aux enfants
        """
        pass

   #---------------------------------------------------
    def onCallNode(self,node):
        """
        pour chaque élément de la sélection, execute la fonction make
        """
        return self.make(node,self.root_obj)

    #-------------------------------------------------
    def get_new_obj(self,**args):
        return getClass(self.cls)(**args)

    #-------------------------------------------------        
    def make(self,data,obj,**args):
        #yield "MAKE "+self.__class__.__name__+" "+self.path()+" on "+data.path()

        data,obj,args=self.onSetArgs(data,obj,**args)
        yield self.onPreMake(data,obj,**args)
        yield self.onMakeChildren(data,obj,**args)
        yield self.onPostMake(data,obj,**args)


    #-------------------------------------------------
    def onSetArgs(self,data,obj,**args):
        obj=self.get_new_obj(parent=obj,name=data.name,selected=data,**args)
        return data,obj,args

    #-------------------------------------------------        
    def onPreMake(self,data,obj,**args):
        pass

    #-------------------------------------------------        
    def onMakeChildren(self,data,obj,**args):

        for child in self.children.by_class(Run):

            if isinstance(child,Builder):
                child.root_obj=obj
                child.set_selection([data,])
                yield child.call()

            elif isinstance(child,Handler):
                child.set_selection([data,])
                yield child.call()

            elif isinstance(child,Run):
                yield child.call()

    #-------------------------------------------------
    def onPostMake(self,data,obj,**args):
        pass
   #---------------------------------------------------
    def onBuild(self,obj):
        pass

    #-------------------------------------------------        
    def get_system(self):
        """
        renvoie le systeme ou erreur si pas de parent systeme
        """

        for elt in self.ancestors:
            if isinstance(elt,Builder):
                return elt
        return self
    #-----------------------------------------------

#==========================================================

