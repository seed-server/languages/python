from dataModel.models import *
from dataModel.structures.network import LinkStore,NodeStore
from .Object import Object
from .Relation import Relation
#==========================================================

class Network(Object):
    
#==========================================================

    NODE_CLS=Field(default=Object)
    LINK_CLS=Field(default=Relation)

    NEW_LINK=Function()
    NEW_NODE=Function()

    #-------------------------------------------------------
    def nodes(self):
        return NodeStore(self.children.by_class(Object).except_class(Relation))

    #-------------------------------------------------------
    def links(self):
        return LinkStore(self.children.by_class(Relation))

    #-------------------------------------------------------
    def new_link(self,source=None,target=None,**args):
        return self.link_cls(parentself,
                        source=self.find(source),
                        target=self.find(target),
                        **args)

    #-------------------------------------------------------
    def new_node(self,**args):
        return self.node_cls(parent=self,**args)
