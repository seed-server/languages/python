from dataModel.models import *
from ..Object import Object

#==========================================================

class Run(Object):
    
#==========================================================
    """
    modèle décrivant un arbre d'execution
    la fonction run permet d'executer un cycle :
        - setup
        - call
        - cleanup

    """


    CYCLES_COUNTER=Integer(default=0,doc="compteur d'appels")

    CALL   =Function(ReturnMerge())
    RUN     =Function(ReturnMerge())
    TEST     =Function()

    def run(self):
        """
        execute le cycle complet en une fois :
            - setup
            - call
            - cleanup
        """
        yield self.setup()
        yield self.call()
        yield self.cleanup()


    def call(self):
        """
        fonction d'execution. Appele
            - onCall
            - onCallChildren
        """


        if self.test()==True:
            self.cycles_counter+=1
            yield self.onCall()
            yield self.onCallChildren()

    def onCall(self):
        """
        code d'execution effectif du modèle
        surcharger pour avoir un comportement
        """
        pass


    def onCallChildren(self):
        """
        code d'execution des enfants
        par défault appele la fonction call des enfants
        surcharger pour avoir un comportement différent
        """
        for elt in self.children.by_class(Run):
            yield elt.call()

    def test(self):
        """
        fonction de test pour savoir si l'execution aura lieu
        """
        if self.active==True:
            return self.onTest()
        return False

    def onTest(self):
        """
        renvoie True si l'execution doit avoir lieu
        surcharge la fonction pour avoir un comportement
        """
        return True
#==========================================================
