import time,threading
from queue import Queue

from .Run import Run
from dataModel.models import *


#===================================================================

class Thread(Run):

#===================================================================
    """
    arbre d'execution avec des noeuds en parralèlle

    """
    REAL_CALL=Function(ReturnMerge())
    START   =Function(ReturnMerge())
    JOIN    =Function(ReturnMerge())
    RESULTS    =Function(ReturnMerge())
    #----------------------------------------------------------------

    def onPostSetup(self):
        """
        à la fin du setup, initialisation du thread
        """
        self.result=None
        self.lock=threading.Lock()
        self.process=threading.Thread(target=self.onExecute)

    #----------------------------------------------------------------
    # BEHAVIOR
    #----------------------------------------------------------------

    def onExecute(self):
        """
        le fonction à executer par le thread
        recupère le résultat dans self.result

        """

        self.result=self.real_call()

    #----------------------------------------------------------------
    def onCall(self):
        """
        si le thread n'a pas démarré, demarrer
        """

        if (self.process is not None) and (not self.process.is_alive() ):
            yield self.start(root=True)

        yield self.join(root=True)
        #yield self.results()

    #----------------------------------------------------------------
    def onCallChildren(self):
        """
        bloque la propagation de call pour dans les descendants
        le thread executera dans sa boucle les descendants
        """
        pass

    #----------------------------------------------------------------

    def real_call(self):
        """
        fonction intermediare pour ajouter le décorateur
        """
        return self.onProcess()


    #----------------------------------------------------------------

    def onProcess(self):
        """
        à surcharger
        par defaut : execute les actions qui ne sont pas des threads.
        """
        for elt in self.children.by_class(Run):
            if isinstance(elt,Thread) != True:

                yield elt.call()

    #----------------------------------------------------------------
    
    def onPostCleanup(self):
        """
        joindre tous les threads
        renvoie les résultats
        """
        yield self.join(root=True)
        yield dict(name=self.name,results=list(self.results()))

    #----------------------------------------------------------------
    # CONTROL
    #----------------------------------------------------------------
    def start(self,root=True):
        """
        demarrer le thread et tous ses enfants
        """


        if root == True and not self.process.is_alive():
            yield self.onStart()
            self.process.start()

        for elt in self.children.by_class(Thread):
            yield elt.start()

    #----------------------------------------------------------------
    def join(self,root=True):
        """
        joindre le thread et tous ses enfants
        """
        for elt in self.children.by_class(Thread):
            if elt.process and elt.process.is_alive():
                yield elt.join()

        if root == True:
            if self.process and self.process.is_alive():

                try:
                    self.process.join()
                    self.process=None
                    yield self.onJoin()
                except KeyboardInterrupt:
                    return "INTERUPT "+self.path()
    #----------------------------------------------------------------

    def results(self):
        """
       renvoie les résultats de l'execution et de tous ses enfants
        """
        if self.result is not None:
            yield self.result
            self.result=None

        for elt in self.children.by_class(Thread):
            yield elt.results()

    #----------------------------------------------------------------

    def onJoin(self):
        """
        fonction executées à la fin de la fonction join()
        quand tous les threads sont finis
        surcharger pour avoir un comportement
        ne fait rien
        """
        return "THREAD JOIN"+self.path()
    #----------------------------------------------------------------
    def onStart(self):
        """
        fonction executées au début de la fonction start()
        avant que tous les threads soient démarrés
        surcharger pour avoir un comportement
        ne fait rien
        """
        return "THREAD START"+self.path()
    #----------------------------------------------------------------
    

#===================================================================

