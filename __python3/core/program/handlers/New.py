from dataModel.models import *
from ..Object import Object
from .Select import Select
from dataModel.Store import Store

#==========================================================

class New(Select):
    
#==========================================================
    """
    permet de créer un enfant pour la ressource sélectionnée
    la classe est écrite dans la variable text

    la varible text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python
    """

    TEXT=String(doc="classe à instancier")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        crée l'instance et l'attache à la ressource
        """
        cls=getClass(self.text%ressource)
        inst=cls(parent=ressource,**dict(self.getNonAttributeDict()))

   #---------------------------------------------------
#==========================================================
