from dataModel.models import *
from ..Object import Object
from .Handler import Handler
from dataModel.Store import Store


#==========================================================

class Select(Handler):
    
#==========================================================
    """
    permet de sélectionner des ressources grace à un descripteur
    dans la variable text

    """

   #---------------------------------------------------
    def onSelect(self):
        """
        crée la sélection à partir du descripteur
        par defaut, selectionne le descripteur
        fonction à surcharger pour avoir un comportement différent
        """

        self.set_selection(self.get_selection())

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        code à executer sur la sélection
        fonction à surcharger pour avoir un comportement
        """
        pass

   #---------------------------------------------------
#==========================================================
