from dataModel.models import *
from dataModel.structures.tree import NamedTree,TreeLoader
from .Select import Select
import os
#==========================================================

class Load(Select,TreeLoader):
    
#==========================================================


    TEXT=String()
    RECURSIVE=Boolean(default=False)
    CLS=String(default="Data")

    #-----------------------------------------------------
    def onCallNode(self,ressource):

        print("    LOAD",self.text%ressource)

        filename=self.text%ressource
        if not os.path.exists(filename):
            filename=os.path.join(os.environ["XMLPYTHON_PATH"],filename)

        if not os.path.exists(filename):
            raise Exception("ERROR no path "+filename)


        yield "SCRIPT "+filename
        yield self.open(filename,ressource,
                first=True,
                recursive=self.recursive,
                extention=".xml",
                cls=self.cls)

    #-----------------------------------------------------
#==========================================================
