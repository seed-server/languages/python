from dataModel.models import *
from ..Object import Object
from .Select import Select
from dataModel.Store import Store

#==========================================================

class Query(Select):
    
#==========================================================
    """
    permet d'd'exuter la méthode de laressource sélectionnée
    la méthode est décrite la variable text

    la varible text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python
    """

    TEXT=String(doc="texte à afficher")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        execute la méthode de la ressource
        """
        text=self.text%ressource
        d=dict()#self.getNonAttributeDict())
        return getattr(ressource,text)(**d)

   #---------------------------------------------------
#==========================================================


