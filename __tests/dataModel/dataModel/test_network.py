from dataModel.models import *

from dataModel.structures.network.NetworkNode import NetworkNode
from dataModel.structures.network.NetworkLink import NetworkLink
from dataModel.structures.network.LinkStore import LinkStore
from dataModel.structures.network.NodeStore import NodeStore


class MyNode(Model,NetworkNode):pass
class MyLink(Model,NetworkLink):pass

def make_network(nodes,links):
    for elt in nodes:
        print(len(elt.inputs),len(elt.outputs))

    print("len",len(links))
    print("sources",len(links.sources()))
    print("targets",len(links.targets()))
    print("nodes",len(links.all_nodes()))
    print("self loops",len(links.selfLoops()))
    print("complete nt",links.targets()==links.sources())
    print("internals",len(links.internals()))
    print("upstreams",len(links.upstreams()))
    print("downstreams",len(links.downstreams()))
    print("borders",len(links.borders()))

    print("inputs",len(nodes.inputs()))
    print("outputs",len(nodes.outputs()))
    print("all_links",len(nodes.all_links()))
    print("internals",len(nodes.internals()))
    print("borders",len(nodes.borders()))

def test():
    nodes=NodeStore()
    for i in range(10):
        nodes.append(MyNode(i=i))

    links=LinkStore()
    for a,b in nodes.carthesian_product(nodes.copy()):
        links.append(MyLink(source=a,target=b))

    make_network(nodes,links)


    nodes1=NodeStore()
    for i in range(10):
        nodes1.append(MyNode(i=i))

    nodes2=NodeStore()
    for i in range(10):
        nodes2.append(MyNode(i=10+i))

    links=LinkStore()
    for a,b in nodes1.carthesian_product(nodes2):
        links.append(MyLink(source=a,target=b))

    make_network(NodeStore(nodes1.union(nodes2)),links)

    nodes=NodeStore()
    links=LinkStore()
    previous=None

    for i in range(10):
        current=MyNode(i=i)
        nodes.append(current)
        if previous:
            links.append(MyLink(source=previous,target=current))
        previous=current

    make_network(nodes,links)


