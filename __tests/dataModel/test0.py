from dataModel import *

class A(Model):

    #field a
    AA=Field()
    #field b
    BB=Field()

    FF=Function()
    GG=Function()

    def ff(self):
        "test"
        return

    def gg(self):
        "test"
        return

class B(metaclass=ModelMeta):

    #field a
    A=Field()
    #field b
    B=Field()

    F=Function()

    def f(self):
        "test"
        return

class C(A,B):

    #field a
    C=Field()
    #field b
    D=Field()

    G=Function()

    def g(self):
        "test"
        return


def print_line(string,level=0):
    s="    "
    print( s*level+string )

def print_doc(cls):

    help=cls.cls_help(cls)
    #print(help)
    for clsname,data in help.items():
        print_line(clsname)
        #print(data["doc"])
        del data["doc"]
        for name,objs in data.items():
            print_line(name,level=1)
            for k,v in objs.items():
                print_line(k,level=2)

print_doc(C)
