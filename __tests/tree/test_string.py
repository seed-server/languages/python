
#from core.program import *
import console
import time
from dataModel.models import *
from core.data import *
from core.strings import *
from core import text_parser
string="""

# $/.name

- $/.class_name
- $/.active

bla bla bla 
bla bla $/.class_name bla 
bla @home bla $/ bla 
bla bla  @ailleurs bla 

## sous titre

$/*/[Data]

## deuxieme

$/**/[Data] -> | {0[class_name]} | {0[name]} |


## deuxieme

$/*/*/[Data] -> - [{0[name]}](./{0[tree_path]})

## deuxieme





"""
def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(3):
            make_tree(cls,level-1,parent=parent)

    return parent

def testX():

    data=make_tree(Data,3)
    p=Processor(parent=data)

    Replace(parent=p,keyword="##",text="\n {} \n"+"="*50+"\n")
    Replace(parent=p,keyword="#",text="#"*50+"\n {} \n"+"#"*50+"\n")
    TreeLineProcessor(parent=p,text="- {0[name]} : {0[tree_path]}")

    w=Worlds(parent=p)
    Replace(parent=w,keyword="bla",text="grumpf")
    Replace(parent=w,keyword="bla",text="grumpf")
    TreeAttrProcessor(parent=w)

    #t=StringTest(parent=w,test_function="startswith",text="$")
    #Replace(parent=t,keyword="#",text="\nune nouvelle variable :\n    {}")
    t=StringTest(parent=w,test_function="startswith",text="@")
    Replace(parent=t,keyword="@",text="\nune nouvelle position :\n    {}")

    console.show_node(p.process(string))

def test():

    data=make_tree(Data,3)
    console.show_node(text_parser.parse(data,string))

