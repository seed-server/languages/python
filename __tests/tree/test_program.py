
#from core.program import *
import console


from dataModel.models import *
from core.program import Object,Run

class MyObject(Object):

    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        return "pre setup "+self.path()#pass
   #---------------------------------------------------


    def onPostSetup(self):
        "overload to have a behavior"
        return "post setup "+self.path()#pass
   #---------------------------------------------------


    def onPreCleanup(self):
        "overload to have a behavior"
        return "pre cleanup "+self.path()#pass
    
   #---------------------------------------------------


    def onPostCleanup(self):
        "overload to have a behavior"
        return "post cleanup "+self.path()#pass

class MyRun(Run):
    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        return "pre setup "+self.path()#pass
   #---------------------------------------------------


    def onPostSetup(self):
        "overload to have a behavior"
        return "post setup "+self.path()#pass
   #---------------------------------------------------


    def onPreCleanup(self):
        "overload to have a behavior"
        return "pre cleanup "+self.path()#pass
    
   #---------------------------------------------------


    def onPostCleanup(self):
        "overload to have a behavior"
        return "post cleanup "+self.path()#pass


    def onCall(self):
        return "call "+self.path()#pass


def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(3):
            make_tree(cls,level-1,parent=parent)

    return parent

def test_tree(cls,level):



    node=make_tree(cls,level)
    node.tree()
    return node

def test():

    objs=test_tree(MyObject,4)

    console.add(objs.setup())
    console.add(objs.cleanup())

    prog=test_tree(MyRun,4)
    console.add(prog.run())

    console.add(help())
